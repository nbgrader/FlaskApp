#! /usr/bin/env bash
# gestion des droits d'acces par ACL (execution avec sudo)
if [ "$#" -eq 0 ]; then
   echo "syntaxe: $0 cours liste_etudiants numero_etudiants exchange_dir"
   exit 1
fi
cours=$1
liste=$2
etudiants=$3
rep="$4/$cours"
# test si rep existe
if [ ! -d $rep ]
then
   mkdir $rep
   chmod 755 $rep
fi
# reset acl
setfacl -b $rep
# par defaut aucun etudiant
setfacl -m o:--- $rep
setfacl -m o:r-x $rep
getfacl -p -t $rep
# forcage ecriture pour cours
chmod 755 $rep
# purge le groupe
#if [ $(getent group $cours) ]; then
#fi
sudo groupdel $cours
sudo groupadd $cours
# liste des etudiants (compte)
rm -f $liste
if [ ! -s $etudiants ]
then
  echo "aucun étudiant dans le cours ($etudiants)"
else
  # sauvegarde du fichiers
  echo "sauvegarde du fichier etudiant (${etudiants}.org)"
  mv -f $etudiants ${etudiants}.org
fi
rm -f $etudiants
