#!/usr/bin/bash
# arret d'un code streamlit
if [ "$#" != "1" ]; then
	echo "syntaxe: $0 script-python"
	exit 1
fi
#
pge=$1
pid=`ps ax | grep "streamlit run" | grep $pge | grep -v grep | awk '{$1=$1; print}'| cut -d " " -f1`
if [ ! -z ${pid// } ]; then
   echo "arret du process streamlit $pge pid=$pid"
   kill -9 $pid
else
   echo "streamlit $pge not running"
fi
rm -f ${pge}.running
