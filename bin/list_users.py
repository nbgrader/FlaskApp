#! /usr/bin/env python3
import os,sys,pwd,grp

cours = os.getcwd()
if len(sys.argv) != 1 :
    cours = sys.argv[1]
print("Liste des étudiants du cours {}".format(cours))
logins=os.listdir(cours+"/submitted/")
#print("liste des étudiants:",logins)
for u in logins:
        p = pwd.getpwnam(u)
        #print(p)
        g = grp.getgrgid(p[3])[0]
        #print(g)
        gecos=p[4].split(' ')
        print(u,g,gecos[0],gecos[1],gecos[1]+'.'+gecos[0]+'@etu.univ-lyon1.fr')
