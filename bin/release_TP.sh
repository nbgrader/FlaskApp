#! /usr/bin/env bash
if [ "$#" -eq 0 ]; then
  echo "generation/release du devoirs nomTP du COURS"
  echo "syntaxe: $0 COURS nomTP"
  exit 1
fi
#
COURS=$1
devoir=$2
DIR0=$PWD
DIRG=$(ls -dx "${PWD}/${COURS}"G*)
if [ -z "$DIRG" ]; then 
  DIRG=${COURS}
else
  echo "liste des groupes du cours $DIRG"
  # creation du lien de configuration (lien vers ssource)
fi
# generation
for dir in $DIRG
do
  cd $dir
  echo "mise a jour du devoir $devoir dans $PWD"
  nbgrader generate_assignment --force $devoir
done
cd ${DIR0}
#
# mise en ligne avec nbgrader
for dir in $DIRG
do
  cd $dir
  echo "release  du devoir $devoir dans $PWD"
  nbgrader release_assignment --force ${devoir}
  nbgrader list
done
cd ${DIR0}
# FIN
