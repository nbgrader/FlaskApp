#!/usr/bin/bash
# status d'un TP streamlit en argument
pge=$1
pid=`ps ax | grep "streamlit run" | grep $pge | grep -v grep`

if [ -z ${pid// } ]; then
  echo -e "TP ${pge} is not running"
  rm -f ${pge}.running
else
  echo -e  "TP ${pge} is running\n ${pid}\n"
  touch ${pge}.running
  echo "Log file ${pge}.log"
  echo "==================="
  tail -10 ${pge}.log
fi
