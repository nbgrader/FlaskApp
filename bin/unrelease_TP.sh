#! /usr/bin/env bash
if [ "$#" -eq 0 ]; then
  echo "un-release de devoirs nomTP du COURS"
  echo "syntaxe: $0 COURS nomTP exchange_dir"
  exit 1
fi
#
COURS=$1
devoir=$2
exchange=$3
DIR0=$PWD
DIRG=$(ls -dx "${COURS}"G*)
if [ -z "$DIRG" ]; then 
	DIRG=${COURS}
fi
echo "liste des groupes du cours $DIRG"
# unrelease  avec nbgrader
for dir in $DIRG
do
  echo "unrelease  du devoir $devoir dans $dir"
  cd ${DIR0}/${dir}
  rm -rf ${exchange}/${dir}/outbound/${devoir}
  #rm -rf ./release/${devoir}
  nbgrader list
done
cd ${DIR0}
# FIN
