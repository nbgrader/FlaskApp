#! /usr/bin/env bash
# gestion des droits d'acces par ACL (execution avec sudo)
if [ "$#" -eq 0 ]; then
   echo "syntaxe: $0 cours liste_etudiants exchange_dir"
   exit 1
fi
cours=$1
etudiants=$2
rep="$3/$cours"
# test si rep existe
if [ ! -d $rep ]
then
   mkdir $rep
   chmod 755 $rep
fi
# reset acl
setfacl -b $rep
# par defaut aucun etudiant
setfacl -m o:--- $rep
# ajoute la liste des etudiants (compte)
if [ ! -s $etudiants ]
then
   echo "aucun étudiant dans le cours ($etudiants)"
else
   # gestion avec un group unix $cours existe
   if [ ! $(getent group $cours) ]; then
     sudo groupadd $cours
   fi
   echo "ajout droit / pour le groupe ${cours}"
   setfacl -m g:${cours}:r-x $rep
   etudiantsCSV=""
   while IFS="" read -r p || [ -n "$p" ]
   do
     if  [[ $p = *[!\ ]* ]]; then
          etudiantsCSV="${etudiantsCSV}$p,"
     fi
   done < $etudiants
   # si l'un des noms donnés à gpasswd est celui d'un user qui n'existe pas, gpasswd ne modifiera rien
   if sudo gpasswd -M $etudiantsCSV $cours; then
      #Succès, rien à faire
      :
   else
      #Echec, probablement un login utilisateur qui n'existait pas. Filtrer les login qui existent
      etudiantsCSV=""
      etudiantsInexistantsCSV=""
      while IFS="" read -r p || [ -n "$p" ]
      do
         if [[ $p = *[!\ ]* ]]; then
            if id $p > /dev/null; then
               etudiantsCSV="${etudiantsCSV}$p,"
            else
               etudiantsInexistantsCSV="${etudiantsInexistantsCSV}$p,"
            fi
         fi
      done < $etudiants
      echo "Login Etudiants Introuvables (ignorés): ${etudiantsInexistantsCSV}"
      sudo gpasswd -M $etudiantsCSV $cours
   fi
fi
# affiche les acl
getfacl -p -t $rep

