#!/usr/bin/bash
# lancement d'un code streamlit (systemd service)
source /var/lib/jupyterhub/venvs/py3/bin/activate
#
pge=$1
port=8501
echo "start TP streamlit ${pge}.py dans "`pwd` "le "`date` 
streamlit run ${pge}.py --server.port  ${port}
