#!/usr/bin/env python3
# creation / mise ajour d'un fichier de notes (manuelles)
# ajout creation fichier csv avec separateur "," pour tomuss
# 
import sys,os
import numpy as np
import pandas as pd
import configparser as ConfigParser
from pathlib import Path
from shutil import copyfile

# test si separateur | (ancienne version)
def check_barsep(fichier):
    line = None
    with open(fichier,"r") as F:
            line = F.readline()
    return '|' in line
#
if len(sys.argv) != 3 :
    print("ERREUR: aucun nom de TP et de cours spécifies")
    sys.exit(1)

nomTP = sys.argv[1]
fichier = "notes_"+nomTP+".csv"
cours = sys.argv[2]
rep = os.getcwd()+"/"+cours+"/validation/"
cfg = os.getcwd()+"/"+cours+"/source/"+nomTP+".cfg"
print("creation/mise ajour du fichier de notes {} dans {}\n".format(fichier,rep))
src = rep+"bilan_"+nomTP+".csv"
dest= rep+fichier
# version avec ","
dest2=rep+nomTP+".csv"
# xml
dest3=rep+nomTP+".xml"
# lecture fichier de configuration
if os.path.isfile(cfg): 
    conf = ConfigParser.RawConfigParser()
    conf.read(cfg)
    # lecture des coefficients
    coef_auto = float(conf.get('NOTE','AUTO'))
    coef_manu = float(conf.get('NOTE','MANU'))
    print("Coefficients note auto={} note manuelle={}\n".format(coef_auto,coef_manu))
else:
    print("ERREUR: le fichier de configuration {} n'existe pas".format(cfg))
    sys.exit(1)
# lecture du fichier de notes automatiques
if os.path.isfile(src): 
    data = pd.read_csv(src,sep=',')
    ID      = data['id' ]
    LOGIN   = data['login']
    ETUDIANT= data['etudiant']
    NOTE    = data['note']
else:
    print("ERREUR: le fichier de notes {} n'existe pas".format(src))
    sys.exit(1)
# creation nouveau dataframe pour notation manuelle
data1 = pd.concat([ID,LOGIN,ETUDIANT,NOTE],axis=1)
data1['comment'] = " commentaire "
data1['auto'] = data1['note'].copy()
data1['manu'] = data1['note'].copy()  
print(data1.info())
if os.path.isfile(dest):
    # lecture des anciennes notes
    print("conservation des anciennes notes dans {}\n".format(dest))
    if check_barsep(dest):
        data2 = pd.read_csv(dest,sep='|')
        data2['comment'] = data2['comment'].str.replace(',',';')
    else:
        data2 = pd.read_csv(dest,sep=',')
    print(data2.info())
    data1['comment'] = data2['comment'].copy()
    data1['manu'] = data2['manu'].copy()
# calcul de la moyenne
data1 = data1.replace('ABINJ',np.nan)
data1['auto'] = data1['auto'].astype(float)
data1['manu'] = data1['manu'].astype(float)
data1['note'] = np.round(coef_auto*data1['auto'] + coef_manu*data1['manu'],2)
data1 = data1.replace(np.nan,'ABINJ')
# sauvegarde du résultat
print("ecriture des notes dans {}".format(dest))
#data1.set_index('id',inplace=True)
#data1.to_csv(dest,index=False,encoding='utf8',sep='|')
data1.to_csv(dest,index=False,encoding='utf8',sep=',')
print(data1.info())
# creation fichier csv avec ","
# remplace les "," dans les commentaires et ajoute une colonne login
data2 = data1.reindex(columns=['id','login','etudiant','note','comment','auto','manu'])
data2['comment'] = data2['comment'].str.replace(',',';')
data2.to_csv(dest2,index=False,encoding='utf8',sep=',')
print("ecriture des notes (avec ,) dans {}".format(dest2))
print(data2.info())
# sauvegarde en xml
data3 = data2[['id','login','note']].copy()
data3.rename(columns={'id':'student','login':'username','note':'score'},inplace=True)
data3['assignment']=nomTP
data3.to_xml(dest3,index=False,root_name="results",row_name="result")
print("ecriture des notes dans {}".format(dest3))
print(data3.info())
# lien dans cours_html
rep_html = os.getcwd()+"/"+cours+"/cours_html/"
Path(rep_html).mkdir(parents=True,exist_ok=True)
dest_html2=rep_html+nomTP+".csv"
if not os.path.exists(dest_html2):
    os.symlink(dest2, dest_html2)
dest_html3=rep_html+nomTP+".xml"
if not os.path.exists(dest_html3):
    os.symlink(dest3, dest_html3)
# fin
sys.exit(0)
