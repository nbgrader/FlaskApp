#! /usr/bin/env bash
# attention execution avec sudo
if [ "$#" -eq 0 ] || [ "$#" -ne 2 ]; then
	echo "syntaxe purge du cours: $0 cours exchange_dir"
	echo "attention: executer avec sudo"
	exit 1
fi
dir=`dirname $0`
cours=$1
exchange=$2
echo "ATTENTION: purge du cours ${cours} et ${exchange}"
cd ${cours}
rm -rf gradebook.db release/ autograded/ submitted/ feedback/ validation/bilan* validation/liste* validation/*csv
rm -rf ${exchange}/${cours}/*
# reset les droits etudiants
$dir/reset_acl.sh ${cours} validation/liste.txt validation/etudiants.txt ${exchange}
