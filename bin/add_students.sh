#! /usr/bin/env bash
FICHIER=validation/liste.txt
if [ "$#" -eq 1 ]; then
	FICHIER=$1
fi
echo "Ajout des etudiants dans nbgraderdb à partir de "$FICHIER
# nbgrader db student list
if [ -z "$(cat ${FICHIER})" ]; then
   echo "liste vide"
   rm ${FICHIER}.csv
else
   # generation liste etudiants avec le nom
   echo " id , last_name" > ${FICHIER}.csv
   getent passwd `cat ${FICHIER}` | awk -F ":" '{print $1 "," $5}' >> ${FICHIER}.csv
   nbgrader db student import ${FICHIER}.csv
fi
nbgrader db student list
