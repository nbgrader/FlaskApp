#! /usr/bin/env bash
FICHIER=validation/liste.txt
if [ "$#" -eq 1 ]; then
	FICHIER=$1
fi
echo "Ajout des etudiants dans nbgraderdb à partir de "$FICHIER
nbgrader db student list
while read id; do
 chaine=`getent passwd $id`
 echo $chaine
 IFS=':' read -r -a array <<< "$chaine"
 echo "add $id : ${array[4]}"
 nbgrader db student add $id --last-name="${array[4]}"
done < $FICHIER
nbgrader db student list
