#! /usr/bin/env python3
''' 
génération de la liste des comptes etudiants pour un TP soit
- a partir du fichier avec les numeros etudiants
conversion numero étudiant -> compte UCB 
- soit s'il n'existe pas = liste des etudiants dans la db de nbgrader 

'''

import os,sys
import subprocess

fichier = sys.argv[1]
# test si le fichier existe sinon on utilise nbgrader
if os.path.exists(fichier):
  # lecture du fichier
  with open(fichier,"r") as F:
    for line in F:
        num = line.strip()
        if num.isdigit():
            fcar = chr(ord('p')+ord(num[0])-ord('1'))
            ident = fcar + num[1:]
            print(ident)
        else:
            print(num)
  print('\n')
  F.close()
else:
  # la cde sur la db ne marche pas avec la nvlle version de nbgrader
  #script="nbgrader db student list | awk '{if (NR!=1) {print \$1}}'"
  #bash = 'bash -c "{}"'.format(script)
  #result = subprocess.check_output(bash, shell=True).decode("utf8") 
  # test si repertoire existe
  if not os.path.exists('./submitted') :
      os.mkdir('./submitted')
  # liste des dossiers dans le reperatoire submitted
  ldirs = next(os.walk('./submitted'))[1]
  result=""
  for nom in ldirs:
    if nom[1:].isdigit():
        result = result + nom + '\n'
  print(result)
# fin
sys.exit(0)
