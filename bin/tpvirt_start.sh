#!/usr/bin/bash
# lancement d'un code streamlit
if [ "$#" != "1" ]; then
	echo "syntaxe: $0 script-python"
	exit 1
fi
#
pge=$1
port=8501
echo "start streamlit ${pge}.py "
nohup streamlit run ${PWD}/${pge}.py --server.port ${port} --server.address localhost >& ${PWD}/${pge}.log &
touch ${pge}.running
sleep 2
echo "Log file ${pge}.log"
echo "==================="
tail -10 ${PWD}/${pge}.log
