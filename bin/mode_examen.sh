#! /usr/bin/env bash
# attention execution avec sudo
# passage d'un cours en mode examen
if [ "$#" -eq 0 ]; then
	echo "syntaxe mode_examen : $0 cours [start/stop]"
	exit 1
fi
source /var/lib/jupyterhub/venvs/py3/bin/activate
cours=$1
mode=$2
echo "mode examen cours ${cours} ${mode}"
cd ${cours}
if [[ ${mode} == "start" ]]; then
   echo "Start exam ${cours}"
   jupyter nbextension enable mode_exam/main --sys-prefix
   if [ -f "header_exam.ipynb" ]; then
      cp -p header_exam.ipynb header.ipynb
   fi
elif [[ ${mode} == "stop" ]]; then
   echo "Stop exam ${cours}"
   jupyter nbextension disable mode_exam/main --sys-prefix
   if [ -f "header_cours.ipynb" ]; then
      cp -p header_cours.ipynb header.ipynb
   fi
else
   echo "Erreur mode inconnu ${mode}"
   exit 1
fi
