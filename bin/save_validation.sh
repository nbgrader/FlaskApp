#! /usr/bin/env bash
# copy des resultats d'un TP dans TP_backup
# utile pour sauvegarder 2 versions de l'évaluation du TP
if [ "$#" -eq 0 ]; then
   echo "syntaxe: $0 cours nomTP"
   exit 1
fi
cours=$1
nomTP=$2
newTP="${nomTP}_backup_${cours}"
echo "sauvegarde evaluation $nomTP dans $newTP pour le cours $cours"
cd $cours
# fichier de configuration et TP
cp     source/${nomTP}.cfg source/${newTP}.cfg
cp -ar source/${nomTP} source/${newTP}
# puis validation
rm -v validation/*${newTP}*
for file in validation/*${nomTP}*
do
	cp $file ${file}.save
	#rename.ul -v  "$nomTP" "$newTP" $file
	rename -v  "$nomTP" "$newTP" $file
        mv ${file}.save $file	
done
# modification des fichier html
for file in validation/bilan_${newTP}.html
do
	echo "modification $file"
	sed -i s/${nomTP}/${newTP}/g $file
done
# copy des resultats des etudiants
rm -v -r submitted/*/${newTP}
for dir in submitted/*/${nomTP}
do
	echo "copy $dir"
	cp -r $dir ${dir}.save
	#rename.ul -v "$nomTP" "$newTP" $dir
	rename -v "$nomTP" "$newTP" $dir
	mv ${dir}.save $dir
done
#
