#!/usr/bin/env bash
if [ "$#" -ne 1 ]; then 
 echo "syntaxe: $0 serveur-nbgrader"
 echo "exemple $0 jupyterl2.mecanique"
 exit 1
fi
host=$1
echo "configuration de validation.html pour le serveur $host"
sed "s/mbuffat-nbgrader/$host/" validation.org >static/validation.html
