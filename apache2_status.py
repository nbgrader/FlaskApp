#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Quick way to monitor Apache mod_status. Designed for use with Zabbix.
from github https://gist.github.com/ninjix

(C) Marc Buffat, dpt mécanique, Lyon 1
"""

import os,sys
import urllib.request
import urllib.error
import argparse
import psutil
import json

def get_url(url):
    """ Uses urllib to request Apache mod_status """
    try:
        conn = urllib.request.urlopen(url)
        response = conn.read()
        conn.close()
        return response

    except urllib.error.HTTPError as e:
        print("Error getting URL ",e.reason)
        sys.exit(1)

def parse_scoreboard(scoreboard):
    """ Parses scoreboard """

    keys = {
        '_': 'WaitingForConnection',
        'S': 'StartingUp',
        'R': 'ReadingRequest',
        'W': 'SendingReply',
        'K': 'KeepaliveRead',
        'D': 'DNSLookup',
        'C': 'ClosingConnection',
        'L': 'Logging',
        'G': 'GracefullyFinishing',
        'I': 'Idle',
        '.': 'OpenSlot'
    }

    scores = {}
    for score in scoreboard:
        if score in keys:
            if keys[score] in scores:
                scores[keys[score]] += 1
            else:
                scores[keys[score]] = 1

    return scores


def get_status(url=None):
    """ Returns an Apache performance stat or list all of them """

    if url is None:
        url = 'http://localhost/server-status'

    stats_text = get_url('{}?auto'.format(url)).decode("utf-8")
    status = {}
    for line in stats_text.split('\n'):
        if ':' in line:
            key, value = line.split(':',1)
            if key == 'Scoreboard':
                for sk,sv in parse_scoreboard(value.strip()).items():
                     status[sk] = sv
            elif key in ['ServerVersion','ServerMPM','Server Built','CurrentTime','RestartTime','ServerUptime',
                         'CacheType','CacheIndexUsage','CacheUsage'] : 
                status[key] = value
            else:
                status[key.strip().replace(' ', '_')] = float(value.strip())
    # etat des services
    status["Jupyter"] = jupyterhub_active()
    status["Apache2"] = apache2_active()
    status["CPUcores"] = "{} cores used {}%".format(psutil.cpu_count(),psutil.cpu_percent())
    status["Memory"]   = "{:.2f} Gb used {}%".format(psutil.virtual_memory().total/(1024**3),psutil.virtual_memory().percent)
    return status


def output_status(status, stat=None):
    """ Output function """
    print("Status serveur local apache2")
    if stat in status:
        print(status[stat])
    else:
        print("\n")
        for key, value in status.items():
            print("{:<30}{:<20}".format(key, value))
        print("\n")

def jupyterhub_active():
    """ check systemd status of jupyterhub """
    return os.system('systemctl is-active --quiet jupyterhub')==0
def apache2_active():
    """ check systemd status of apache2 """
    return os.system('systemctl is-active --quiet apache2')==0

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    parser.add_argument('-u', '--url', help='URL to query stats', default='http://localhost/server-status')
    parser.add_argument('-s', '--stat', help='Return the value for a specific statistic', default=None)
    parser.add_argument('-l', '--list', help='List all available Apache mod_status stats', action='store_true')
    parser.add_argument('--json', help='Dump output as json', action='store_true')
    args = parser.parse_args()

    srv_status = get_status(args.url)
    if args.json :
        print(json.dumps(srv_status))
    else:
        output_status(srv_status, stat=args.stat)
