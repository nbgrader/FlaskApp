"""Flask configuration."""
# Configuration file holding default values.
# DO NOT MODIFY FOR A SPECIFIC SERVER, make a config.py file next to this one and override the desired values
# Only values in uppercase are actually stored in the config object later on. So make sure to use uppercase letters for your config keys.
from os import environ, path

TESTING = False
DEBUG = False
ENV = 'production'
SECRET_KEY = 'a definir'
# flask
FLASKPATH = "/home/cours/FlaskApp/"
FLASKURL = "flask_cours"
ENV = "/var/lib/jupyterhub/venvs/py3"
# attention enlever /usr/local/Validation dans les nvlles installations
PATH = "/var/lib/jupyterhub/venvs/py3/bin:"+FLASKPATH+"/bin:"
#PATH = "/var/lib/jupyterhub/venvs/py3/bin:/usr/local/Validation:"+FLASKPATH+"/bin:"
# nbgrader
EXCHANGE = "/srv/nbgrader/exchange/"
# info sur GPU (cde)
GPUSTAT = None 
GPURUN  = None
# liste des cours geres (lien dans static)
BASEDIR  = "/home/cours/"
# liste des COURS à gerer dans le meta-cours
COURS = []
# monitoring liste de machine (optionnel)
#HOSTS=[jupyterm2.mecanique.univ-lyon1.fr]
HEADERIMAGE = 'images/LogoUCBL.png'
FOOTERIMAGE = 'images/LogoUCB.png'
FOOTERIMAGEALTTEXT = 'Logo UCBL'
FOOTERLINKTEXT = 'Plateforme Jupyter Include'
FOOTERLINKURL = 'https://jupyter.univ-lyon1.fr/'
