from setuptools import setup

setup(
    name='validation_FlaskApp',
    version='1.0',
    author='Marc BUFFAT',
    description='gestion de cours sous Jupyter nbgrader',
    long_description='gestion de cours sous Jupyter nbgrader',
    url='https://forge.univ-lyon1.fr/nbgrader/FlaskApp',
    keywords='',
    python_requires='>=3.7, <4',
    install_requires=[
        'jupyter',
        'nbgrader',
    ],
)
