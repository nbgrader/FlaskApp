#! /usr/bin/env python3
# (C) Marc BUFFAT département mécanique, UCB Lyon 1
# interface WEB pour les outils de validation
import os,sys
import psutil
import subprocess
import glob
from   pathlib import Path
import pandas as pd
import shutil
import tarfile
import datetime, time
import requests
import logging
import json
from threading import Thread
from systemdlogging.toolbox import init_systemd_logging

from flask import Flask, render_template, flash, session, abort, send_from_directory, request, redirect, jsonify, Response
from flask_bootstrap import Bootstrap
from flask_nav import Nav
from flask_nav.elements import Navbar,View, Subgroup
from flask_fontawesome import FontAwesome

# bibliotheques locales 
import apache2_status
from flaskapp_util import list_students, list_cours, list_admins, active_users, active_servers, arret_server, list_pedagos, info_user #, list_Pedagos
# barre navigation
topbar = Navbar(
               View('<Jupyter', 'get_jupyter'),
               View('Gestion',  'get_gestion'),
               View('Cours', 'get_cours'),
               View('TP',    'get_tp'),
               Subgroup(
                   'Docs',
                    View('Documentation',   'get_doc'),
                    View('Guide enseignant', 'get_nbgrader'),
                    View('FAQ',   'get_faq'),
                    View('Bibliothéques', 'get_bib'),
               ),
               Subgroup(
                   'Admin',
                    View('Status Jupyterhub', 'get_status'),
                    View('Serveurs Etudiants', 'get_students'),
                    View('Log Apache', 'log_server'),
                    View('Log Exam', 'log_exam'),
                    View('Status Apache', 'status'),
                    View('Serveurs Jupyter', 'get_servers'),
                    View('About', 'get_about'),
               ),
              )
# Link('link','url')
nav = Nav()
nav.register_element('top',topbar)
# interface WEB pour la validations de notebook
app = Flask(__name__, instance_relative_config=True)
Bootstrap(app)
app.config.from_pyfile('config-default.py')
app.config.from_pyfile('config.py')
app.secret_key = app.config['SECRET_KEY']
# fonts
fa = FontAwesome(app)
app.config['FONTAWESOME_STYLES'] = ['brands','solid']
# configuration
ENV     = app.config['ENV']
PYTHON  = ENV+"/bin/python3"
my_env = os.environ.copy()
my_env["PATH"] = app.config['PATH'] + my_env["PATH"]
my_env["VIRTUAL_ENV"] = ENV
# variables globales (en majuscule) 
SECRET = app.config['SECRET_KEY']
# attention identique pour chaque utilisateur
# les variables locales à l'utilisateur sont dans session
DEBUG   = app.config['DEBUG']
# gestion log
init_systemd_logging()  # Returns True if initialization went fine.
logger = logging.getLogger(__name__)
if DEBUG:
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)
#
BASEDIR = app.config['BASEDIR']
#if DEBUG: print("config: ",ENV,PYTHON," MYENV=",my_env," basedir=",BASEDIR," DEBUG=",DEBUG)
# nbgrader
EXCHANGE = app.config['EXCHANGE']
# GPUSTAT
GPUSTAT = app.config['GPUSTAT']
GPURUN = app.config['GPURUN']
# FLASK
FLASKPATH = app.config['FLASKPATH']
FLASKURL  = app.config['FLASKURL']
logger.info("start flask app:"+FLASKPATH+FLASKURL)
# liste des cours potentiels
LCOURS = app.config['COURS']
# liste des cours sur le serveur
listcours = list_cours(SECRET)
# liste des cours du meta-cours 
LISTCOURS = []
for cours in LCOURS:
    if cours in listcours: LISTCOURS.append(cours)
logger.info("liste des cours:"+str(LISTCOURS))
# determine liste des administarteurs
ADMINS = list_admins(SECRET)
logger.info("liste des administrateurs:"+str(ADMINS))
# determine la liste des equipes pédagogiques
#PEDAGOS = list_Pedagos(LISTCOURS,BASEDIR)
PEDAGOS = list_pedagos(SECRET)
logger.info("liste des enseignants:"+str(PEDAGOS))
# determine la liste des TP / cours
TPLIST = {}
for cours in LISTCOURS:
    TPLIST[cours] =  [f.split("/")[-1].split(".")[:-1][0] for f in sorted(glob.glob(BASEDIR+cours+"/source/*cfg"))]
#
logger.info("liste des TPs:"+str(TPLIST))
# determine la liste des TPvirtuels
TPVIRTLIST = {}
for cours in LISTCOURS:
    # liste de TP virtuel avec streamlit
    TPVIRTLIST[cours] =  [f.split("/")[-1].split(".")[:-1][0] for f in sorted(glob.glob(BASEDIR+cours+"/source/*toml"))]
logger.info("liste des TPVirtuels:"+str(TPVIRTLIST))
# determine la liste de TPvirtuels a relancer
TPRUNLIST = {}
for cours in LISTCOURS:
    # liste de TP virtuel avec streamlit a relancer
    TPRUNLIST[cours] =  [f.split("/")[-1].split(".")[:-1][0] for f in sorted(glob.glob(BASEDIR+cours+"/cours/*running"))]
    for TP in TPRUNLIST[cours]:
        script = "(cd {}/cours/; tpvirt_stop.sh {}; tpvirt_start.sh {})".format(cours,TP,TP)
        bash = 'bash -c "{}"'.format(script)
        result = subprocess.check_output(bash, shell=True, env=my_env ).decode("utf8")
        logger.info("start TP streamlit {}: {}".format(TP,result))
#
# execute script python
def run_pyscript(script,arg,Flash=DEBUG):
    result="Erreur execution du script python : "+script
    try:
      pyscript = PYTHON+' '+script+' '+arg  
      result = subprocess.check_output(pyscript, shell=True, env=my_env ).decode('utf8')
      if Flash: flash("python script execution OK","info")
    except subprocess.CalledProcessError as err:
      logger.error("erreur code:{} output:{}".format(err.returncode,err.output))
      flash("python script {} \nerreur code{}\n {}".format(pyscript,err.returncode,err.output),"danger")
    return result
# execute une ligne de code python
def run_pyline(line,Flash=DEBUG):
    result="Execution code python : "+line
    try:
       pyline = PYTHON+' -c "' + line + '"' 
       result = subprocess.check_output(pyline, shell=True, env=my_env ).decode('utf8')
       if Flash: flash("python inline execution OK","info")
    except subprocess.CalledProcessError as err:
       logger.error("erreur code:{} output:{}".format(err.returncode,err.output))
       flash("python inline {}\n erreur code{}\n {}".format(pyline,err.returncode,err.output),"danger")
       result="Erreur execution code python : "+line
    return result
# run script bash
def run_bash(script,Flash=DEBUG):
    if Flash: logger.info("exec  cde: "+script)
    result="Execution script : "+script
    try:
       bash = 'bash -c "{}"'.format(script)
       result = subprocess.check_output(bash, shell=True, env=my_env ).decode("utf8")
       if Flash: flash("bash script execution OK","info")
    except subprocess.CalledProcessError as err:
       logger.error("erreur code:{} output:{}".format(err.returncode,err.output))
       flash("bash script erreur code {} {}".format(err.returncode,err.output),"danger")
       result="Erreur execution du script : "+script
    return result
# generation d'une liste de fichier d'un repertoire
def make_tree(path):
    tree = dict(name=os.path.basename(path), children=[])
    try: lst = os.listdir(path)
    except OSError:
        pass #ignore errors
    else:
        for name in lst:
            fn = os.path.join(path, name)
            if os.path.isdir(fn):
                tree['children'].append(make_tree(fn))
            else:
                tree['children'].append(dict(name=name))
    return tree
# test si un fichier existe et si oui le détruit si il est vide
def test_empty(fichier):
    if os.path.exists(fichier):
        f = open(fichier,'r')
        content = f.read().strip()
        f.close()
        if content == '' :
            os.remove(fichier)
            logger.debug("remove : {}".format(fichier))
            return True
        return False
    return True
# test si fichier de configuration du tp existe et le crée sinon
DEFAULT_CFG = """
[IPYNB]
nomipynb = {}.ipynb
cellids = cell_all
[PDF]
nompdf = 
[TEX]
nomtex = 
[BIB]
nombib = 
exos = 
[PGE]
nompge = 
[MALUS]
similaire = 5 
taux = 0.8
[NOTE]
base = 0.0
bib  = 0.0
ipynb= 1.0
auto = 0.5
manu = 0.5
"""
def check_cfg(tp,path):
    fichier = path + "{}.cfg".format(tp)
    # test si fichier de configuration est vide (presque)
    if (not os.path.exists(fichier)) or os.stat(fichier).st_size < 10 :
        # création du fichier
        logger.info("création fichier de configuration: "+fichier)
        F = open(fichier,"w")
        F.write(DEFAULT_CFG.format(tp))
        F.close()
    return
#
# executions de scripts TP
# ========================
#
ERROR_NOCOURS ="Vous devez sélectionner un cours sur la page Gestion dans la liste Validation des cours"
ERROR_NOTP    ="Vous devez sélectionner un TP sur la page cours dans la liste Sélection des TP"
# bilanTP avec autograde
@app.route('/bilanTPautograde')
def bilanTPautograde():
    global PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if 'tp' not in session: abort(404, description=ERROR_NOTP)
    TP = session['tp']
    if session['username'] not in PEDAGOS[Cours] : 
        abort(403, description="1 {} n est pas responsable du cours".format(session['username']))
    script = "(cd {}; bilanTP -a -l validation/liste_{}.txt -b validation {})".format(Cours,TP,TP) 
    result = run_bash(script)
    return render_template('script_long.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)  
# version sans autograde
@app.route('/bilanTP')
def bilanTP():
    global PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if 'tp' not in session: abort(404, description=ERROR_NOTP)
    TP = session['tp']
    if session['username'] not in PEDAGOS[Cours] : 
        abort(403, description="2 {} n est pas responsable du cours".format(session['username']))
    script = "(cd {}; bilanTP -l validation/liste_{}.txt -b validation {})".format(Cours,TP,TP) 
    result = run_bash(script)
    return render_template('script_long.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)  
# version avec sortie pdf 
@app.route('/bilanTPpdf')
def bilanTPpdf():
    global PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if 'tp' not in session: abort(404, description=ERROR_NOTP)
    TP = session['tp']
    if session['username'] not in  PEDAGOS[Cours] : 
        abort(403, description="3 {} n est pas responsable du cours".format(session['username']))
    script = "(cd {}; bilanTP --pdf -l validation/liste_{}.txt -b validation {})".format(Cours,TP,TP) 
    result = run_bash(script)
    return render_template('script_long.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)  
# version avec sortie pandoc pdf
@app.route('/bilanTPpandoc')
def bilanTPpandoc():
    global PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if 'tp' not in session: abort(404, description=ERROR_NOTP)
    TP = session['tp']
    if session['username'] not in PEDAGOS[Cours] : 
        abort(403, description="4 {} n est pas responsable du cours".format(session['username']))
    script = "(cd {}; bilanTP --pandoc -l validation/liste_{}.txt -b validation {})".format(Cours,TP,TP) 
    result = run_bash(script)
    return render_template('script_long.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)  
# version debug
@app.route('/bilanTPdebug')
def bilanTPdebug():
    global PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if 'tp' not in session: abort(404, description=ERROR_NOTP)
    TP = session['tp']
    if session['username'] not in PEDAGOS[Cours] : 
        abort(403, description="5 {} n est pas responsable du cours".format(session['username']))
    script = "(cd {}; bilanTP -d -a -l validation/liste_{}.txt -b validation {})".format(Cours,TP,TP) 
    result = run_bash(script)
    return render_template('script_long.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)  
# etudiant pour un tp
@app.route('/add_etudiants_tp/<TP>')
def add_etudiants_tp(TP):
    global PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if session['username'] not in PEDAGOS[Cours] : 
        abort(403, description="6 {} n est pas responsable du cours".format(session['username']))
    if 'tp' not in session: abort(404, description=ERROR_NOTP)
    TP = session['tp']
    flist = 'validation/liste.txt'
    ftp = 'validation/liste_{}.txt'.format(TP)
    # optimisation 
    #if os.path.exists(Cours+"/"+flist) and os.path.exists(Cours+"/"+ftp) and (os.stat(Cours+"/"+flist).st_size == os.stat(Cours+"/"+ftp).st_size) :
    #        shutil.copy(Cours+"/"+flist,Cours+"/"+ftp)
    #        return get_tp()
    # fin optimisation
    #script = '(cd {}; sed "s/^./p/" validation/etudiants.txt > {}; add_newline.sh {}; add_students.sh {})'.format(COURS,flist,flist,flist)
    script = '(cd {}; numero_id.py validation/etudiants.txt > {};  add_students.sh {})'.format(Cours,flist,flist)
    result = run_bash(script)
    shutil.copy(Cours+"/"+flist,Cours+"/"+ftp)
    return render_template('script.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)

# ajoute les étudiants a partir des notebooks
@app.route('/add_notebooks')
def add_notebooks():
    global PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if 'tp' not in session: abort(404, description=ERROR_NOTP)
    TP = session['tp']
    if session['username'] not in PEDAGOS[Cours] : 
        abort(403, description="7 {} n est pas responsable du cours".format(session['username']))
    script = '(cd {}; liste_notebooks.sh {} validation/liste_{}.txt; add_students.sh validation/liste_{}.txt)'.format(Cours,TP,TP,TP)
    result = run_bash(script)
    return render_template('script.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)
# recupere les TP
@app.route('/collect')
def collect():
    global PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if 'tp' not in session: abort(404, description=ERROR_NOTP)
    TP = session['tp']
    if session['username'] not in PEDAGOS[Cours] : 
        abort(403, description="8 {} n est pas responsable du cours".format(session['username']))
    # collecte des TP
    # =============
    script='(cd {}; nbgrader collect --update {})'.format(Cours,TP)
    result = run_bash(script)
    #
    # mise a jour de la  liste des etudiants (cours/tp/..)
    # ==========
    fetu  = 'validation/etudiants.txt'
    # check if file empty and remove
    test_empty(Cours+"/"+fetu)
    flist = 'validation/liste.txt'
    # check if file empty and remove
    test_empty(Cours+"/"+flist)
    ftp   = 'validation/liste_{}.txt'.format(TP)
    # optimisation 
    #if os.path.exists(Cours+"/"+flist) and os.path.exists(Cours+"/"+ftp) and (os.stat(Cours+"/"+flist).st_size == os.stat(Cours+"/"+ftp).st_size) :
    #    shutil.copy(Cours+"/"+flist,Cours+"/"+ftp)
    #    result += "Optimisation: cp {} dans {}\n".format(flist,ftp)
    #else:
    script = '(cd {}; numero_id.py {} > {}; add_students.sh {})'.format(Cours,fetu,flist,flist)
    result += run_bash(script)
    shutil.copy(Cours+"/"+flist,Cours+"/"+ftp)
    result += "Creation {} puis cp {} dans {}\n".format(flist,flist,ftp)
    # fin optimisation
    return render_template('script.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)
# bilan  notes,similitudes sur tous les groupes de TP
@app.route('/bilan_GrpTP')
def bilan_GrpTP():
    global PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if 'tp' not in session: abort(404, description=ERROR_NOTP)
    TP = session['tp']
    if session['username'] not in PEDAGOS[Cours] : 
        abort(403, description="9 {} n est pas responsable du cours".format(session['username']))
    # liste des etudiants
    flist = 'validation/liste.txt'
    ftp = 'validation/liste_{}.txt'.format(TP)
    result = ""
    # optimisation 
    #if os.path.exists(Cours+"/"+flist) and os.path.exists(Cours+"/"+ftp) and (os.stat(Cours+"/"+flist).st_size == os.stat(Cours+"/"+ftp).st_size) :
    #    shutil.copy(Cours+"/"+flist,Cours+"/"+ftp)
    #else:
    #script = '(cd {}; sed "s/^./p/" validation/etudiants.txt > {}; add_newline.sh {}; add_students.sh {})'.format(COURS,flist,flist,flist)
    script = '(cd {}; numero_id.py validation/etudiants.txt > {}; add_students.sh {})'.format(Cours,flist,flist)
    result += run_bash(script)
    shutil.copy(Cours+"/"+flist,Cours+"/"+ftp)
    # fin optimisation
    # liste des cours de TP associés au cours
    CoursTP = ""
    LST = glob.glob(Cours+"G*")
    for nom in LST:
        CoursTP += " " + nom
    if CoursTP == "": abort(404, description="aucun cours de TP associé à {}".format(Cours))
    # bilan notes et similitude 
    script = "(cd {}; bilanTP -a -l validation/liste_{}.txt -b validation {} --simil {})".format(Cours,TP,TP,CoursTP) 
    result += run_bash(script)
    return render_template('script_long.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)
# validation auto d'un TP avec ou sans autograde (mode=autograde)
@app.route('/valide_auto/<mode>')
def valide_auto(mode):
    global PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if 'tp' not in session: abort(404, description=ERROR_NOTP)
    TP = session['tp']
    if session['username'] not in PEDAGOS[Cours] : 
        abort(403, description="10 {} n est pas responsable du cours".format(session['username']))
    #
    # mise a jour de la  liste des etudiants (cours/tp/..)
    result=""
    fetu  = 'validation/etudiants.txt'
    # check if file empty and remove
    test_empty(Cours+"/"+fetu)
    flist = 'validation/liste.txt'
    # check if file empty and remove
    test_empty(Cours+"/"+flist)
    ftp   = 'validation/liste_{}.txt'.format(TP)
    # optimisation 
    script = '(cd {}; numero_id.py {} > {}; add_students.sh {})'.format(Cours,fetu,flist,flist)
    result += run_bash(script)
    shutil.copy(Cours+"/"+flist,Cours+"/"+ftp)
    result += "Creation {} puis cp {} dans {}\n".format(flist,flist,ftp)
    # validation avec autograde
    # =========================
    logfile = "{}/validation/cde_{}.log".format(Cours,TP)
    if mode == "autograde":
        script = "(date; cd {}; bilanTP -a -l {} -b validation {})>{}&".format(Cours,ftp,TP,logfile) 
    else :
        script = "(date; cd {}; bilanTP -l {} -b validation {})>{}&".format(Cours,ftp,TP,logfile) 
    result += "Execution bash script {}\n".format(script)
    result += run_bash(script)
    time.sleep(10)
    return render_template('script_long.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)
#
# scripts cours
# =============
#
# configuration auto d'un cours
@app.route('/config_auto')
def config_auto():
    global PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if session['username'] not in PEDAGOS[Cours] : 
        abort(403, description="11 {} n est pas responsable du cours".format(session['username']))
    flist = 'validation/liste.txt'
    #script = '(cd {}; sed "s/^./p/" validation/etudiants.txt > {}; add_newline.sh {}; add_students.sh {})'.format(COURS,flist,flist,flist)
    script = '(cd {}; numero_id.py validation/etudiants.txt > {}; add_students.sh {})'.format(Cours,flist,flist)
    result = run_bash(script)
    script = '(cd {}; set_acl.sh {} {} {})'.format(Cours,Cours,flist,EXCHANGE)
    result += run_bash(script)
    # ajout des enseignants
    script = "("
    for prof in PEDAGOS[Cours]:
        script += 'setfacl -m u:{}:r-x {};'.format(prof,EXCHANGE+Cours)
    script += ")"
    logger.debug("exec script:{}".format(script))
    result += run_bash(script)
    return render_template('script_cours.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)  
# definit les droits acl
@app.route('/set_acl')
def set_acl():
    global PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    script = '(cd {}; set_acl.sh {} validation/liste.txt {})'.format(Cours,Cours,EXCHANGE)
    result = run_bash(script)
    # ajout des enseignants
    script = "("
    for prof in PEDAGOS[Cours]:
        script += 'setfacl -m u:{}:r-x {};'.format(prof,EXCHANGE+Cours)
    script += ")"
    logger.debug("set_acl script:{}".format(script))
    result += run_bash(script)
    return render_template('script_cours.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)
@app.route('/reset_acl')
def reset_acl():
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    script = '(cd {}; reset_acl.sh {} validation/liste.txt validation/etudiants.txt {})'.format(Cours,Cours,EXCHANGE)
    result = run_bash(script)
    return render_template('script_cours.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)
# ajoute les étudiants a partir de la liste des inscrits
@app.route('/add_etudiants')
def add_etudiants():
    global PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if session['username'] not in PEDAGOS[Cours] : 
        abort(403, description="12 {} n est pas responsable du cours".format(session['username']))
    flist = 'validation/liste.txt'
    #script = '(cd {}; sed "s/^./p/" validation/etudiants.txt > {}; add_newline.sh {} add_students.sh {})'.format(COURS,flist,flist,flist)
    script = '(cd {}; numero_id.py validation/etudiants.txt > {}; add_students.sh {})'.format(Cours,flist,flist)
    result = run_bash(script)
    return render_template('script_cours.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)
# liste des etudiants
@app.route('/list_users')
def list_users():
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    script=FLASKPATH+'bin/list_users.py'
    result = run_pyscript(script,Cours)
    return render_template('script_cours.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)
@app.route('/list_db')
def list_db():
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    script="(cd {}; nbgrader db student list)".format(Cours)
    result = run_bash(script)
    return render_template('script_cours.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)
# menus de base
@app.route('/')
def get_gestion():
    try:
        user = session['username']
    except:
        abort(403,description="vous devez vous identifier")
    if 'listcours' not in session: abort(403, description=ERROR_NOCOURS)
    LISTcours = session['listcours']
    InterPro = session['interpro']
    return render_template('index.html', interpro=InterPro, listcours=LISTcours, appconfig=app.config)
# idem mais changement mode interface
@app.route('/change_interpro', methods=['GET'])
def change_interpro():
    logger.debug("set interface pro={}".format(session['interpro']))
    session['interpro'] = not session['interpro']
    return get_gestion()
# cours
@app.route('/cours', methods=['GET'])
def get_cours():
    global PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if session['username'] not in PEDAGOS[Cours] : 
        abort(403, description="14 {} n est pas responsable de cours".format(session['username']))
    InterPro = session['interpro']
    if 'tplist' not in session: abort(404, description="Erreur: aucune liste de TP dans le cours")
    TPlist = session['tplist']
    TPvirt = session['tpvirt']
    # creation repertoire validation 
    valdir   = BASEDIR + Cours + '/validation'
    if not os.path.exists(valdir): os.makedirs(valdir)
    etufile  = valdir+'/etudiants.txt' 
    # test si le cours est ouvert à tous ou pas
    #cours_ouvert = test_empty(etufile) 
    cours_ouvert = not os.path.exists(etufile)
    #if not os.path.exists(etufile): Path(etufile).touch()
    TARlist=[os.path.basename(f) for f in glob.glob(BASEDIR + Cours + "/archives/*tar.gz")]
    return render_template('cours.html',admin=PEDAGOS[Cours],cours=Cours,TP_list=TPlist,TP_virt=TPvirt,TAR_list=TARlist,interpro=InterPro,cours_open=cours_ouvert, appconfig=app.config)
@app.route('/set_cours/<cours>')
def set_cours(cours):
    session['cours']  = cours
    TPlist =  [f.split("/")[-1].split(".")[:-1][0] for f in sorted(glob.glob(BASEDIR+cours+"/source/*cfg"))]
    session['tplist'] = TPlist
    # liste de TP virtuel avec streamlit
    TPvirt =  [f.split("/")[-1].split(".")[:-1][0] for f in sorted(glob.glob(BASEDIR+cours+"/source/*toml"))]
    session['tpvirt'] = TPvirt
    return get_cours()

# TP
@app.route('/tp', methods=['GET'])
def get_tp():
    global PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if session['username'] not in PEDAGOS[Cours] : 
        abort(403, description="13 {} n est pas responsable de cours".format(session['username']))
    InterPro = session['interpro']
    if 'tp' not in session: abort(404, description=ERROR_NOTP)
    TP = session['tp']
    check_cfg(TP,BASEDIR+Cours+"/source/")
    return render_template('tp.html',cours=Cours,tp=TP,fichier="bilan_"+TP+".csv",fichier1="notes_"+TP+".csv",
                           fichier2="bilan_similitude_"+TP+".csv",fichierlog="cde_"+TP+".log",
                           interpro=InterPro,basedir="static", appconfig=app.config)
# idem mais avec set_tp (donc url #)
@app.route('/set_tp/<tp>')
def set_tp(tp):
    session['tp'] = tp
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    InterPro = session['interpro']
    if 'tp' not in session: abort(404, description=ERROR_NOTP)
    TP = session['tp']
    check_cfg(TP,BASEDIR+Cours+"/source/")
    return render_template('tp.html',cours=Cours,tp=TP,fichier="bilan_"+TP+".csv",fichier1="notes_"+TP+".csv",
                           fichier2="bilan_similitude_"+TP+".csv",fichierlog="cde_"+TP+".log",
                           interpro=InterPro,basedir="../static", appconfig=app.config)
# gestion TP virtuel
@app.route('/status_tpvirt/<tp>')
def status_tpvirt(tp):
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    TP = tp
    script = "(cd {}/cours/; tpvirt_status.sh {})".format(Cours,TP) 
    #script = "sudo -u cours XDG_RUNTIME_DIR=/run/user/1023 systemctl --user status {}.service".format(TP)
    result = run_bash(script)
    return render_template('script.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)  
@app.route('/start_tpvirt/<tp>')
def start_tpvirt(tp):
    if 'cours' not in session: abort(404, description=ERROR_NOTP)
    Cours = session['cours']
    TP = tp
    script = "(cd {}/cours/; tpvirt_stop.sh {}; tpvirt_start.sh {})".format(Cours,TP,TP) 
    #script = "XDG_RUNTIME_DIR=/run/user/$(id -u cours) systemctl --user start {}.service".format(TP)
    result = run_bash(script)
    return render_template('script.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)  
@app.route('/stop_tpvirt/<tp>')
def stop_tpvirt(tp):
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    TP = tp
    script = "(cd {}/cours/; tpvirt_stop.sh {})".format(Cours,TP) 
    #script = "XDG_RUNTIME_DIR=/run/user/$(id -u cours) systemctl --user stop {}.service".format(TP)
    result = run_bash(script)
    return render_template('script.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)  

@app.route('/tpvirt/<tp>')
def tpvirt(tp):
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    InterPro = session['interpro']
    TP = tp
    return render_template('tpvirt.html',cours=Cours,tp=TP,interpro=InterPro, appconfig=app.config)
# release et unrelease
@app.route('/release_tp/<tp>')
def release_tp(tp):
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    TP = tp
    script = "(release_TP.sh {} {})".format(Cours,TP) 
    result = run_bash(script)
    return render_template('script_long.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)  
@app.route('/unrelease_tp/<tp>')
def unrelease_tp(tp):
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    TP = tp
    script = "(unrelease_TP.sh {} {} {})".format(Cours,TP,EXCHANGE) 
    result = run_bash(script)
    return render_template('script_long.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)  
# affichage d'un fichier texte dans validation
@app.route('/content/<file>', methods=['GET'])
def content(file):
    Cours = session['cours']
    fichier = Cours + "/validation/"+file
    logger.info("start display content of "+fichier)
    result = "Erreur lecture fichier : "+fichier
    if os.path.exists(fichier):
        script = "(cat {})".format(fichier)
        result = run_bash(script)
    return render_template('content.html', text=result)
# affichage des notes
#
@app.route('/notes/<file>', methods=['GET'])
def notes(file):
    result=""
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if 'tp' not in session: abort(404, description=ERROR_NOTP)
    TP = session['tp']
    fichier = Cours + "/validation/"+file
    # mise a jour du TP des notes manuels
    if "notes_" in file:
        script=FLASKPATH+'bin/gestion_notes.py'
        result = run_pyscript(script,TP+" "+Cours)
    if os.path.exists(fichier):
        data = pd.read_csv(fichier,sep=",")
        data.set_index('id',inplace=True)
        result = data.to_html(classes='table table-striped')
    else:
        flash("le fichier {} n existe pas".format(fichier),"danger")
    return render_template('notes.html',cours=Cours,tp=TP,notes=result, appconfig=app.config)
# exportation tomuss des notes (colonne notes du fichier)
@app.route('/tomuss/<file>', methods=['GET'])
def tomuss(file):
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    fichier = Cours + "/validation/"+file
    ID    = []
    Notes = []
    if os.path.exists(fichier): 
        data = None
        if "notes_" in file:
            #data = pd.read_csv(fichier,"|")
            data = pd.read_csv(fichier,sep=",")
        else:
            data = pd.read_csv(fichier,sep=",")
        ID    = data['id' ].to_list()
        Notes = data['note'].to_list()
    else:
        flash("le fichier {} n existe pas".format(fichier),"danger")
    return render_template('tomuss.html',IdNotes=zip(ID,Notes), appconfig=app.config)
# exportation tomuss des commentaires (colonne comment du fichier)
@app.route('/tomuss_comment/<file>', methods=['GET'])
def tomuss_comment(file):
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    fichier = Cours + "/validation/"+file
    ID      = []
    Comment = []
    if os.path.exists(fichier): 
        #data = pd.read_csv(fichier,sep="|")
        data = pd.read_csv(fichier,sep=",")
        ID    = data['id' ].to_list()
        Comment = data['comment'].to_list()
    else:
        flash("le fichier {} n existe pas".format(fichier),"danger")
    return render_template('tomuss.html',IdNotes=zip(ID,Comment), appconfig=app.config)
# creation d'un fichier de notes TP pour notation manuelle
@app.route('/notesTP/<nomtp>', methods=['GET'])
def notesTP(nomtp):
    TP = nomtp
    Cours = session['cours']
    script=FLASKPATH+'bin/gestion_notes.py'
    result = run_pyscript(script,TP+" "+Cours)
    return render_template('script.html', run_script=script, res_script=result, cours=TP, appconfig=app.config)
# generation liste repertoire path
@app.route('/dir/<path>')
def dirtree(path):
    return render_template('dirtree.html', tree=make_tree(path), appconfig=app.config)

# liste des bibliothéques Python
@app.route('/bib',methods=['GET'])
def get_bib():
    # test si connecté
    try:
        user = session['username']
    except:
        abort(403,description="vous devez vous identifier")
    # liste des bibliotheques
    PIP        = run_bash("pip list| sed '1,2d'",Flash=False)
    # config
    cde = 'grep "c.SystemdSpawner.*_limit"  '+ENV+'/etc/jupyter/jupyterhub_config.py'
    LIMIT      = run_bash(cde, Flash=False)
    CPU        = "{} cores used {}%".format(psutil.cpu_count(),psutil.cpu_percent())
    MEMORY     = "{:.2f} Gb used {}%".format(psutil.virtual_memory().total/(1024**3),psutil.virtual_memory().percent)
    HOST       = request.host
    return render_template('bib.html',host=HOST,python=sys.version,pip=PIP,limit=LIMIT,cpu=CPU,memory=MEMORY, appconfig=app.config)

# about
@app.route('/about', methods=['GET'])
def get_about():
    # test si connecté
    try:
        user = session['username']
    except:
        abort(403,description="vous devez vous identifier")
    # affiche configuration
    EXPORT = run_bash('export',Flash=False)
    PY_ENV = os.environ
    PY_PATH= sys.path
    PY_SUB = sys.executable
    PWD    = os.getcwd()
    JUPYTERHUB = run_bash('jupyterhub --version',Flash=False)
    JUPYTER    = run_bash('jupyter --version',Flash=False)
    NBGRADER   = run_bash('nbgrader --version',Flash=False)
    PIP        = run_bash('pip list',Flash=False)
    cde = 'grep "c.SystemdSpawner.*_limit"  '+ENV+'/etc/jupyter/jupyterhub_config.py'
    LIMIT      = run_bash(cde, Flash=False)
    CPU        = "{} cores used {}%".format(psutil.cpu_count(),psutil.cpu_percent())
    MEMORY     = "{:.2f} Gb used {}%".format(psutil.virtual_memory().total/(1024**3),psutil.virtual_memory().percent)
    HOST       = request.host
    return render_template('about.html',ses=session,syspath=sys.path,osenv=os.environ,pyexe=sys.executable,
                            pypath=PY_PATH,export=EXPORT,python3=PY_SUB,pyenv=PY_ENV,pwd=PWD,
                            jupyterhub=JUPYTERHUB, jupyter=JUPYTER, nbgrader=NBGRADER,pip=PIP,
                            cpu=CPU,memory=MEMORY,host=HOST,limit=LIMIT, appconfig=app.config)
# doc
@app.route('/doc', methods=['GET'])
def get_doc():
    #return send_from_directory('static','documentation.html')
    #return render_template('documentation.html', appconfig=app.config)
    return redirect("https://perso.univ-lyon1.fr/marc.buffat/COURS/BOOK_VALIDATION_HTML/intro.html")
# documentation utilisateur
@app.route('/nbgrader', methods=['GET'])
def get_nbgrader():
    return send_from_directory('static','doc_nbgrader.html')
    #return render_template('doc_nbgrader.html', appconfig=app.config)
@app.route('/faq', methods=['GET'])
def get_faq():
    return render_template('faq.html', appconfig=app.config)
# loggin (plus de passage en claire): 
# attention ne pas mettre le nom de l'univesité i.e. nom.prenom et non nom.prenom@univ-lyon1.fr
@app.route('/logging', methods=['GET'])
def logging():
    global PEDAGOS, LISTCOURS

    #logger.debug("start logging session:")
    try:
        user = session['user']
    except Exception as err:
        logger.debug("error logging "+str(err))
        abort(403,description="aucune gestion de cours autorisée")
    logger.debug("logging user:"+user)
    listcours = []
    for cours in LISTCOURS:
        if user in PEDAGOS[cours]:
            listcours.append(cours)
    if len(listcours) :
        session['listcours'] = listcours
    else:
        abort(403,description="l'utilisateur "+user+" n'est membre de l'équipe pédagogique d'aucun cours")
    session['username'] = user
    session['interpro'] = False
    logger.info("flaskapp used by :"+user)
    return render_template('index.html',interpro=False, listcours=listcours, appconfig=app.config)
# jupyter
@app.route('/jupyter', methods=['GET'])
def get_jupyter():
    host = request.host
    url = 'https://'+host
    if 'cours'  in session: 
        Cours = session['cours']
        url = 'https://'+host+"/services/"+Cours+"/lab?"
    logger.debug("url:"+url)
    return redirect(url)

# edit fichier dans le repertoire validation
@app.route('/edit/<fichier>',methods=['GET'])
def edit(fichier):
    Cours = session['cours']
    valdir   = BASEDIR + Cours 
    etufile  = valdir + "/validation/" + fichier 
    if not os.path.exists(etufile): 
        Path(etufile).touch()
        logger.debug("creation fichier etudiant "+etufile)
    url = 'https://'+ request.host + '/services/{}/lab/tree/validation/{}'.format(Cours,fichier)
    logger.debug("url:"+url)
    return redirect(url)

# sauvegarde dans un fichier tar zip
@app.route('/archivage', methods=['GET'])
def archivage():
    if 'cours' not in session: abort(404, description="pas de cours selectionné")
    Cours = session['cours']
    x = datetime.datetime.now()
    # creation repertoire  archives
    valdir   = BASEDIR + Cours + '/archives'
    if not os.path.exists(valdir): os.makedirs(valdir)
    # creation archive
    archive = "{}/validation_{}.tar.gz".format(Cours,x.strftime("%d_%b_%Y"))
    # efface les archives precedentes
    #for f in glob.glob("COURS/validation*.tar.gz"):
    #    os.remove(f)
    with tarfile.open(archive,"w:gz") as f:
        fichier = '{}/gradebook.db'.format(Cours)
        if os.path.isfile(fichier):
            f.add(fichier)
        for root, dirs, files in os.walk('{}/validation'.format(Cours)):
            for fichier in files:
                f.add(os.path.join(root, fichier))
        for root, dirs, files in os.walk('{}/submitted'.format(Cours)):
            for fichier in files:
                f.add(os.path.join(root, fichier))
    f.close()
    script = "(tar -tvf {}; mv {} {})".format(archive,archive,valdir) 
    result = run_bash(script)
    return render_template('script.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)  
# purge cours
@app.route('/purge', methods=['GET'])
def purge():
    global FLASKPATH
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    script = "(sudo {}/bin/purge_cours.sh {} {})".format(FLASKPATH,Cours,EXCHANGE) 
    result = run_bash(script)
    return render_template('script.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)  

# sauvegarde evaluation d'un TP
@app.route('/save_validation', methods=['GET'])
def save_validation():
    global FLASKPATH, PEDAGOS
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if 'tp' not in session: abort(404, description=ERROR_NOTP)
    TP = session['tp']
    if session['username'] not in PEDAGOS[Cours] : 
        abort(403, description="8 {} n est pas responsable du cours".format(session['username']))
    script = "({}/bin/save_validation.sh {} {})".format(FLASKPATH,Cours,TP) 
    result = run_bash(script)
    return render_template('script.html', run_script=script, res_script=result, cours=Cours, appconfig=app.config)  
#
# page de log
@app.route('/log_server')
def log_server():
    script = "journalctl -u apache2.service -n 200 --no-pager -o short -r"
    result = run_bash(script)
    #return jsonify(result)
    return render_template('status_log.html', cde="journalctl",res=result, appconfig=app.config)
@app.route('/log_exam')
def log_exam():
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    #script = "journalctl -u apache2.service -n 100 --no-pager -o short -r --grep='EXAMEN' --case-sensitive | { grep "+Cours+" || test $? = 1; }"
    script = "journalctl -u apache2.service -n 100 --no-pager -o short -r --grep='EXAMEN' --case-sensitive | grep "+Cours
    result = run_bash(script)
    #return jsonify(result)
    return render_template('status_log.html', cde="journalctl EXAMEN "+Cours,res=result, appconfig=app.config)
# ajout d'un log
@app.route('/log_add', methods=['POST', 'GET'])
def log_add():
    try:
        data = request.get_json()
        message  = data["mess"]
        dateTime = data["date"]
        hostname = data["host"]
        url_path = data["url"].split('/')
        user  = url_path[2]
        nom,prenom,num_id = info_user(user)
        cours = url_path[-3]
        tp    = url_path[-2]
        mess  = "[EXAMEN] {}({} {}) {} {} {}".format(user,nom,prenom,cours,tp,message)
    except:
        mess = str-request.data
    logger.info(mess)
    return Response("OK")
#
# rest API
#
@app.route('/status', methods = ['GET'])
def status():
    '''API status machine'''
    if (request.method == 'GET'):
        data = apache2_status.get_status('http://localhost/server-status')
        cde = 'grep "c.SystemdSpawner.*_limit"  '+ENV+'/etc/jupyter/jupyterhub_config.py'
        data['Limite'] = run_bash(cde, Flash=False)
        data['Users']  = active_users(SECRET)
        if GPUSTAT is not None:
            data['Gpustat'] = run_bash(GPUSTAT, Flash=False)
        else :
            data['Gpustat'] = None 
        if GPURUN is not None:
            data['Gpurun'] = run_bash(GPURUN, Flash=False)
        else :
            data['Gpurun'] = None 
        return jsonify(data)
# affichage du status par machine 
@app.route('/get_status', methods = ['GET'])
def get_status():
    # test si connecté ADMIN ou PEDAGOS
    if session['username'] not in ADMINS : 
      try:
        global PEDAGOS
        if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
        Cours = session['cours']
        if session['username'] not in PEDAGOS[Cours] : 
            abort(403, description=" {} n est autorisé".format(session['username']))
      except:
        abort(403,description="vous n'etes pas autorisé")
    Hosts = [request.host]
    if 'HOSTS' in app.config: Hosts += app.config['HOSTS']
    Stat = {}
    for host in Hosts:
        logger.info("get status from:"+host+'/flask_cours/status')
        try:
            response = requests.get('https://'+host+'/flask_cours/status')
            logger.info("response: "+response.text)
            # response.raise_for_status()
            Stat[host] = json.loads(response.text)
        except:
            logger.error('Erreur status request: {} '.format(host))
            Stat[host] = None
    return render_template('status.html',hosts=Hosts,status=Stat,admins=ADMINS, appconfig=app.config)
# affiche la liste des serveurs actifs
@app.route('/servers', methods = ['GET'])
def get_servers():
    # test si connecté
    try:
        if session['username'] not in ADMINS : 
            abort(403, description=" {} n est autorisé".format(session['username']))
    except:
        abort(403,description="vous n'etes pas autorisé")
    # liste des servers actifs
    Host = request.host
    data = active_servers(SECRET)
    logger.debug("active servers:"+str(data))
    return render_template('servers.html',host=Host,servers=data, appconfig=app.config)
# affiche la liste des serveurs actifs etudiants
@app.route('/servers_etu', methods = ['GET'])
def get_servers_etu():
    # test si connecté
    try:
        user = session['username']
    except:
        abort(403,description="vous devez vous identifier")
    # liste des servers actifs
    Host = request.host
    # selection uniquement des étudiants
    data = active_servers(SECRET,True)
    logger.debug("active student servers:"+str(data))
    return render_template('servers.html',host=Host,servers=data, appconfig=app.config)
# affiche la liste des servers etudiants d'un cours
@app.route('/students', methods = ['GET'])
def get_students():
    global PEDAGOS
    # test si connecté
    try:
        user = session['username']
    except:
        abort(403,description="vous devez vous identifier")
    if 'cours' not in session: abort(404, description=ERROR_NOCOURS)
    Cours = session['cours']
    if session['username'] not in PEDAGOS[Cours] : 
        abort(403, description="14 {} n est pas responsable de cours".format(session['username']))
    # liste des servers actifs
    Host = request.host
    # selection uniquement des étudiants du cours
    data = list_students(Cours,logger) 
    logger.debug("list student  cours :"+str(Cours)+str(data))
    return render_template('students.html',host=Host,cours=Cours,students=data, appconfig=app.config)
# arrete un serveur
@app.route('/stop_server/<server>')
def stop_server(server):
    status = arret_server(SECRET,server) 
    return render_template('status_cde.html', cde="stop_server",res=status, info="Serveur /user/"+server, appconfig=app.config)
#
# debut du pge
#
nav.init_app(app)
# display configuration
#if DEBUG: print(app.config)
#
if __name__ == "__main__":
    app.run(host='127.0.0.1', port=5000)
