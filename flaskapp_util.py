#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
fonctions utiles pour l'application flaskapp

(C) Marc BUFFAT
"""

import requests
import os,sys
from datetime import datetime
from datetime import timezone
import dateutil.parser
from nbgrader.apps import NbGraderAPI
from traitlets.config import Config
import pwd, getpass
import random

# configuration
api_url = 'http://127.0.0.1:8081/hub/api/'

pageSize = 200 #There seems to be a limit of 200 users per request allowed "api_page_default_limit"

# retourne les information pour un etudiant
def myHash(text:str):
    ''' hash in python '''
    hash=0
    for ch in text:
        hash = ( hash*281  ^ ord(ch)*997) & 0xFFFFFFFF
    return hash
# determine le numero etudiant 
def numero_etu(login):
    ''' calcul le numero etudiant en fonction du login'''
    num_etu = login[1:]
    if num_etu.isdigit() :
        # conversion 1er caractere
        fcar = chr(ord('1')+ord(login[0])-ord('p'))
        num_etu = int(fcar + num_etu)
    else:
        # etudiant non UCB (hash value 7 digits)
        #num_etu = hash(login[:]) % (10**8)
        num_etu = myHash(login)
    return num_etu
# information nom prenom d'un utilisateur
def info_user(login=None):
    ''' renvoie nom,prenom numero étudiant d'un utilisateur (courant par defaut) '''
    if login is None:
        login = getpass.getuser()
    p = pwd.getpwnam(login)
    # enleve les , (unix passwd)
    gecos=p[4].replace(',','').split(' ')
    nom = gecos[0]
    prenom = nom
    if len(gecos) > 1 :
        prenom = gecos[1]
    if len(gecos) > 2 :
        # ignore dernier champs
        nom = ' '.join(gecos[0:-2])
        prenom = gecos[-2]
    num_id = numero_etu(login)
    return nom,prenom,num_id
# liste des etudiants d'un cours
def list_students(cours,logger=None):
    '''détermine la liste des étudiants d'un cours'''
    # create a custom config object to specify options for nbgrader
    config = Config()
    config.CourseDirectory.course_id = cours
    config.CourseDirectory.root = os.getcwd()+"/"+cours
    if logger is not None: logger.info("cours dir "+str(config.CourseDirectory.root))
    #config.CourseDirectory.root définit a partir de static 
    api = NbGraderAPI(config=config)
    data = api.get_students()
    # lire le fichier etudiants.txt dans cours/validation
    fichier="{}/validation/etudiants.txt".format(cours)
    etudiants=[]
    if os.path.exists(fichier):
        with open(fichier,'r') as fr:
            lines = fr.read()
            etudiants = lines.splitlines()
    if logger is not None: logger.info("etudiants: "+str(etudiants))
    #
    students={}
    for user in data:
      if (user['id'] in etudiants) or (user['id'][1:]).isdigit() : 
          if user['last_name'] is not None:
            students[user['id']]=user['last_name']
    # trie par clé  
    #students_trie = { k:students[k] for k in sorted(students.keys()) }
    # trie par valeurs (nom)
    students_trie = dict(sorted(students.items(), key=lambda item: item[1]))
    return students_trie 

# liste des cours
def list_cours(token):
    '''determine la liste des cours sur le serveur'''
    # 
    r = requests.get(api_url+'services', headers={ 'Authorization': 'token %s' % token, })
    r.raise_for_status()
    # liste des services de cours
    data = r.json()
    listcours = []
    for service in data:
        name = data[service]['name']
        cde  = data[service]['command']
        if 'jupyterhub-singleuser' in cde:
            listcours.append(name)
    return listcours
# liste des utilisateurs actifs
def active_users(token):
    '''determine la liste des utilisateurs actifs sur le serveur'''
    # 
    r = requests.get(api_url+'users?state=active', headers={ 'Authorization': 'token %s' % token, })
    r.raise_for_status()
    # liste des services de cours
    data = r.json()
    return len(data) 
# liste des serveurs actifs
def active_servers(token,student_only=False):
    """liste des serveurs actifs"""
    r = requests.get(api_url+'users?state=active', headers={ 'Authorization': 'token %s' % token, })
    r.raise_for_status()
    data = r.json()
    servers={}
    for user in data:
      if (not student_only) or (user['name'][1:]).isdigit() : 
        last_activity = datetime.now(timezone.utc) - dateutil.parser.isoparse(user['last_activity'])
        sec = last_activity.seconds
        hours, remainder = divmod(sec, 3600)
        minutes, seconds = divmod(remainder, 60)
        last_time = '{:02}h {:02}m {:02}s'.format(int(hours), int(minutes), int(seconds))
        servers[user['name']]=[user['server'],last_time]
    # trie
    servers_trie = { k:servers[k] for k in sorted(servers.keys()) }
    return servers_trie 
# arret d'un server
def arret_server(token,server):
    r = requests.delete(api_url+'users/{}/server'.format(server), headers={ 'Authorization': 'token %s' % token, })
    r.raise_for_status()
    if r.text != "": 
        return r.text
    else:
        return "arret OK"
# test API Jupyter
def test_api(token,service):
    # 
    r = requests.get(api_url+service, headers={ 'Authorization': 'token %s' % token, })
    r.raise_for_status()
    data = r.json()
    return data
# determine la liste des administrateurs
def list_admins(token):
    ADMINS=[]
    offset=0
    usersReceived = pageSize
    # lecture par page
    while usersReceived == pageSize :
        r = requests.get(api_url+'users'+f"?offset={offset}&limit={pageSize}", headers={ 'Authorization': 'token %s' % token, })
        r.raise_for_status()
        # liste des services de cours
        data = r.json()
        for user in data:
            if (user['kind'] == 'user') and (user['admin']):
                ADMINS.append(user['name'])
        usersReceived = len(data)
        offset = offset + pageSize
    #
    return ADMINS 
# determine la liste des equipes pédagogiques par cours (API)
def list_pedagos(token):
    # 
    r = requests.get(api_url+'groups', headers={ 'Authorization': 'token %s' % token, })
    r.raise_for_status()
    data = r.json()
    PEDAGOS = {}
    for group in data:
        try:
            cours = group['name']
            if group['users'] :
                PEDAGOS[cours]=group['users']
        except:
            print("Erreurs pas d'équipe pédagagogique pour ",group)
            sys.exit(1)
    return PEDAGOS 
# determine la liste des équipes pédagogiques par cours (lecture fichier de configuration)
# ancienne methode
def list_Pedagos(LISTCOURS,BASEDIR):
    ''' determine la liste des equipes pédagogiques par cours'''
    ADMIN = {}
    for cours in LISTCOURS:
        try:
          users = []
          # nouvelle version hub_config
          f = open(BASEDIR+cours+"/hub_config.py",'r',encoding='utf8')
          line = f.readline()
          while line:
              if "enseignants = " in line:
                  user = f.readline()
                  while "]" not in user:
                      users.append(user.strip().replace("'","").replace(",",""))
                      user = f.readline()
                  line = f.readline()
              elif "admins = " in line:
                  user = f.readline()
                  while "]" not in user:
                      users.append(user.strip().replace("'","").replace(",",""))
                      user = f.readline()
                  break
              else:
                  line = f.readline()
          f.close()
          # ancienne version hub_config
          if len(users) == 0 :
            f = open(BASEDIR+cours+"/hub_config.py",'r',encoding='utf8')
            line = f.readline()
            while line:
              if "name:" in line:
                  user = f.readline()
                  while "]" not in user:
                      users.append(user.strip().replace("'","").replace(",",""))
                      user = f.readline()
                  break
              else:
                  line = f.readline()
            f.close()
          # test errer
          if len(users) == 0 :
              print("Erreurs pas d'équipe pédagagogique pour ",cours,users)
              sys.exit(1)
          else:
              ADMIN[cours] = users[:]
        except OSError as err:
          print("Erreur {} lecture de {} ".format(err,BASEDIR+cours+"hub_config.py "))
          sys.exit(1)
    # fin
    return ADMIN
#
if __name__ == '__main__':
    SECRET_KEY = 'X8m5h64J8q'
    print("Test API Jupytherhub")
    data = list_cours(SECRET_KEY)
    print("cours \n",data)
    data = active_users(SECRET_KEY)
    print("active users\n",data)
    data = list_pedagos(SECRET_KEY)
    print("equipes pedagogiques\n",data)
    data = list_admins(SECRET_KEY)
    print("admin: \n",data)
    data = test_api(SECRET_KEY,'services')
    print("API data:",data)
    #cours = 'MGCtest'
    cours = input("nom du cours = ")
    print("liste etudiants du cours:",cours)
    if os.path.exists(cours):
        data = list_students(cours)
        print("students:\n",data)
    else:
        print("ATTENTION executer le script dans static ")

