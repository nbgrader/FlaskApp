#! /usr/bin/env python3
from nbgrader.apps import NbGraderAPI
from traitlets.config import Config

# create a custom config object to specify options for nbgrader
config = Config()
config.CourseDirectory.course_id = "MGC1061M"
config.CourseDirectory.root = "static/MGC1061M"

api = NbGraderAPI(config=config)

res = api.get_students()
# affiche la liste
for user in res:
    print(user['id'],user['last_name'])
