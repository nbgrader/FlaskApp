# Serveur Nbgrader

nbgrader est un outil qui facilite la création et la notation de devoirs utilisant des notebooks Jupyter, ainsi que la distribution ainsi
que la récupération de ces devoirs aux étudiants. 
Il permet aux enseignants de créer facilement des devoirs basés sur des notebooks qui incluent à la fois des exercices de programmation,
du texte explicatif, des images ou des vidéos, et des réponses libres écrites. 
nbgrader fournit également une interface simplifiée pour gérer les devoirs.

## documentation officiel: 

- [https://nbgrader.readthedocs.io/en/stable/](https://nbgrader.readthedocs.io/en/stable/)
- documentation en français d'un enseigant de lycée
   - [https://www.lecluse.fr/info/jupyter/nbgrader/](https://www.lecluse.fr/info/jupyter/nbgrader/)

## utilisation coté étudiant
Quand vous vous connectez sur un serveur nbgrader, vous avez par défaut un rôle étudiant, et vous avez accès aux versions étudiants des notebooks.

Pour cela allez dans le menu **Assignements**, puis choisir le cours dans la boite de dialogue
 *Released, downloaded, and submitted assignments for course:*

Voua avez alors un affichage de 3 cases:

1. **Released assignments** qui affiche la liste des notebooks du cours que vous pouvez télécharger (en cliquant sur **fetch**)

2. **Downloaded assignments** qui affiche la liste des notebooks du cours que vous avez téléchargé. Vous pouvez ensuite soumettre votre notebook pour évaluation en cliquant sur **submit**

3. **Submitted assignments**  qui affiche la liste des notebooks du cours que vous avez soumis avec la date de soumission.

**REMARQUES**
 - un notebook téléchargé ne peut plus être téléchargé à nouveau (il faut dans ce cas supprimer le répertoire du notebook sur votre compte sur le serveur qui se trouve dans le dossier: NOM_COURS->NOM_TP)

  - la commande **validate** permet de passer tout les tests sur le notebook et renvoie une erreur en cas d'erreur (en général à ne pas le faire utiliser par les étudiants)

 - un même notebook peut être soumis plusieurs fois: seul la dernière version est prise en compte (à utiliser avec modération car cela occupe de l'espace disque inutilement)


Pour réinitialiser la configuration nbgrader de l'utilisateur, on peut effacer le repertoire ~/.local/share/jupyter et .jupyter:

        rm -rf ~/.local/share/jupyter .jupyter 


## utilisation coté enseignant

Pour avoir accès au cours coté enseignant, il faut avoir été inscrit dans l'équipe pédagogique du cours. Dans ce cas, il faut cliquez sur le bouton en haut à gauche **Control Panel**, puis sur **Services** et choisir dans la liste le nom du cours (code APOGEE).

On a alors accès au répertoire du cours et à tous les fichiers du cours et à la gestion des notebooks pour les étudiants.

### structure d'un cours

Un cours sous Jupyter nbgrader est associé à un répertoire sur le serveur. Le nom du cours est par convention son code APOGEE. Par exemple le répertoire du cours **MGC2005L** a la structure suivante

- répertoire du cours **MGC2005L**

<p><span style="display:block;background:lightyellow;">
	<b>MGC2005L/</b><br>
	├── <i>autograded</i><br>
	│   ├── p1801574<br>
	│   │   └── TP4_Meule<br>
	│   ├── p1811042<br>
	│   │   └── TP4_Meule<br>
	│   └── p2006068<br>
	│       &nbsp;&nbsp;&nbsp; └── TP4_Meule<br>
	├── <i>release</i><br>
	│   ├── TP4_Meule<br>
	│   └── TP5_Bille<br>
	├── <b>source</b><br>
	│   ├── TP4_Meule<br>
	│   └── TP5_Bille<br>
	├── <i>submitted</i><br>
	│   ├── p1801574<br>
	│   │   └── TP4_Meule<br>
	│   ├── p1811042<br>
	│   │   ├── TP3_Toupie3D<br>
	│   │   ├── TP4_Meule<br>
	│   │   └── TP5_Bille<br>
	│   └── p2006068<br>
	│       &nbsp;&nbsp;&nbsp; ├── TP4_Meule<br>
	│       &nbsp;&nbsp;&nbsp; └── TP5_Bille<br>
	└── <b>validation</b><br>
</span></p>

Les répertoires en italique sont crées automatiquement par **nbgrader**

*release*
: contient les TP mis en ligne par l'enseignant responsable du cours ( menu Formgrader de l'interface jupyter nbgrader)

*submitted*
: contient les TP qui ont été soumis par les étudiants. Pour chaque étudiant on trouve les TP soumis par cet étudiant

*nbgrader*
: contient le résultat de l'évaluation des TP par le système nbgrader, avec la même structure par étudiant

Les répertoires en gras sont générés par l'administrateur

- **source** contient les TP que l'enseignant fournit aux étudiants (dans l'exemple TP4_Meule et TP5_Bille)

- **validation** contient les résultats  de l'analyse avec les outils de validation pour chacun des TP

### répertoire des notebooks

Les notebooks de cours, devoirs ou de TP sont placés dans des sous-répertoires du répertoire source. Chaque sous-répertoire, p.e. TP4_Meule, représente un TP ou devoir, et peut contenir un ou plusieurs fichiers de notebooks (extension .ipynb), des fichiers images ou vidéo, des bibliothèques python, des fichiers de données ou des documents au format pdf.

Lorsqu'un étudiant récupère un TP ou devoir (**fetch**), il récupère la totalité du répertoire avec tous les fichiers.

De même, lorsqu'un étudiant soumet son travail (**submit**), il transmet la totalité des fichiers du répertoire.

### gestion des notebooks

On utilise le menu **Formgrader**

#### Manage Assignements

Dans l'option **Manage Assignements**, on gère la mise a disposition et la récupération des devoirs. Les différentes étapes sont:

1. **Generate** génération de la version étudiant du notebook
2. **Preview** possibilité de visualisé cette version avant de la mettre en ligne
3. **Release** mise à disposition des notebooks aux les étudiants
4. **Collect** récupération des notebooks soumis par les étudiants
5. **Submission** nbre de notebooks récupérés

#### Manage Students

Gestion des étudiants (avec les notes) avec une notation automatique (**autograde**).


#### Manual grading

Notation manuelle des étudiants

**remarque** la gestion et la notation des notebooks peut se faire de façon plus efficace et plus simple avec le système de **validation** de cours mise en place sur les serveurs, qui permet
aussi la gestion des étudiants et le transfert des notes vers tomuss.

## Bibliographie

1. [Teaching and Learning with Jupyter (jupyter book)](https://jupyter4edu.github.io/jupyter-edu-book/)
2. [How to Use Jupyter Notebook: A Beginner’s Tutorial](https://www.dataquest.io/blog/jupyter-notebook-tutorial/)
3. [Jupyter documentation](https://docs.jupyter.org/en/latest/)
