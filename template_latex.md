---
title: Documentation Nbgrader pour enseignant
author: Thomas Dupriez et Marc Buffat, UCB Lyon 1 
header-includes: |
    \usepackage{pmboxdraw}
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyhead[CO,CE]{}
    \fancyfoot[CO,CE]{}
    \fancyfoot[LE,RO]{\thepage}
abstract: |
    Documentation sur l'utilisation de Jupyter Nbgrader 
    et des outils de gestion de cours par un enseignant 

...

