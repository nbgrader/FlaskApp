#! /usr/bin/env bash
echo "building documentation and FAQ"
if [ "$#" -eq 1 ] && [[ "$1" == '-pdf' ]]; then
echo "generation pdf"
pandoc -s --toc --number-sections -t pdf --pdf-engine=xelatex template_latex.md doc_enseignant.md doc_nbgrader.md  -o static/doc_nbgrader.pdf
#pandoc -s -t html5 documentation.md  -o documentation.pdf
fi
# option -N pour numeroter toc
pandoc -s --toc --toc-depth 3 --template template.html -t html doc_flaskapp.md  -o templates/documentation.html
pandoc -s --toc --toc-depth 3 --template template.html -t html faq.md  -o templates/faq.html
pandoc --self-contained --toc --toc-depth 3 --template template_static.html -t html doc_enseignant.md doc_nbgrader.md -o static/doc_nbgrader.html
