---
title: Documentation enseignant
---

# Guide Enseignant

Vous pouvez télécharger [la version pdf de ce guide (ici)](static/doc_nbgrader.pdf)

## Définitions

- **Jupyter** est une application web pour programmer. Notre installation supporte Python et Octave comme langages de programmation. Cette application contient un système de fichiers dans lequel l'utilisateur peut mettre ce qu'il veut (dossiers, fichiers textes, images...) mais tout particulièrement des notebooks.
- Un **Notebook** (calepin, carnet) est un document contenant un ensemble de cellules. Ces cellules peuvent contenir du code, du texte, des images et d'autres choses. Le code d'une cellule peut être exécuté pour voir son résultat directement. Un notebook python aura l'extension de fichier *.ipynb*.
- **JupyterHub** est une plateforme permettant de fournir des instances de Jupyter à plusieurs utilisateurs.
- **NbGrader** est un outil déployé sur notre plateforme JupyterHub pour fournir des services tel que la notation automatique de notebooks.
- Dans ce document, un **TP** est un notebook python ainsi que ses fichiers supports (images, jeux de données...). A noter qu'il s'agit juste d'un nom, rien n'empêche de créer un TP sur la plateforme et de s'en servir comme support de cours distribuable aux étudiants.
- Dans ce document, un **cours** possède un code (par exemple PHY2020) et contient un ensemble de TPs. Chaque cours définit la liste des étudiants qui ont accès à ses TPs.

## Se Connecter

- Liens vers la plateforme : 
	- **L1:** [https://jupyterl1.mecanique.univ-lyon1.fr](https://jupyterl1.mecanique.univ-lyon1.fr/)
	- **L2:** [https://jupyterl2.mecanique.univ-lyon1.fr](https://jupyterL2.mecanique.univ-lyon1.fr)
	- **L3:** [https://jupyterl3.mecanique.univ-lyon1.fr](https://jupyterl3.mecanique.univ-lyon1.fr)
	- **M1:** [https://jupyterm1.mecanique.univ-lyon1.fr](https://jupyterm1.mecanique.univ-lyon1.fr)
	- **M2:** [https://jupyterm2.mecanique.univ-lyon1.fr](https://jupyterm2.mecanique.univ-lyon1.fr)
- Votre login/motdepasse est le même que celui de votre compte université Lyon 1

![login](./images/login.png)

## Les Ecrans de la Plateforme
### Votre Espace Jupyter Personnel 

- Après la connexion, vous verrez votre espace Jupyter personnel. Il est équivalent à ceux qu'ont les étudiants. **Ce n'est pas ici qu'il faut créer les ressources du cours**, mais vous pouvez tout de même créer et exécuter des notebooks python ici.
- Cliquez sur "Control Panel" pour accéder à l'espace enseignant.

![EspacePersonnel](./images/EspacePersonnel_edit.png)

### Panneau de Contrôle JupyterHub


- Le seul élément à considérer sur cet écran est le bouton **"Services"** en haut à gauche, il permet d’accéder aux applications de gestion de ces UEs. Les applications de gestion ont des noms qui commencent par **"Portail"**, et elles gèrent les UEs qui sont situées en dessous.

![EspaceJupyterHub](./images/EspaceJupyterHub_edit.png)

- En cliquant sur un portail on accède à une page avec la liste des cours de ce portail.
Cette page contient uniquement les cours, dans lesquels vous faites partie de l'équipe pédagogique et donc dont vous 
pouvez accéder aux ressources pédagogiques.

### Espace Jupyter d'un Cours

![EspaceUE](./images/EspaceUE_edit.png)

Éléments importants:

- Le bouton **Formgrader** permet d’accéder à l'écran de gestion des TPs du cours. Voir section *"Ecran de Gestion des TPs d'un Cours"*.
- Le dossier **source** contient les TPs.
- Le fichier **header.ipynb** est un notebook python qui est intégré comme en-tête en haut des notebooks qui seront transmis aux étudiants.
- Un fichier de configuration par TP (fichier *.cfg*), mainteant dans le répertoire source.
- Le fichier **hub_config.py** contient la liste des enseignants autorisés à accéder à cette UE. Voir section *Ajouter un Enseignant à un Cours*.

#### Ecran de Gestion des TPs d'un Cours

![GestionTP](./images/GestionTP.png)

Cet écran montre les TPs déclarés dans le cours, et permet de les rendre accessibles aux étudiants. Voir section *"Cycle de Vie d'un TP"* pour apprendre comment l'utiliser.

On accède à cet écran en cliquant sur le bouton *"formgrader"* de l'espace Jupyter du cours.

### Application de Gestion de Cours
Une application de gestion de cours peut gérer plusieurs cours (la capture d'écran suivante montre une application qui ne gère qu'un seul cours).

![](./images/ApplicationGestionCours1_edit.png)

Cet écran permet :

- d'accéder à l'écran de gestion d'un cours en particulier géré par cette application (première liste de liens "Validation de cours")
- de naviguer vers l'espace jupyter d'un cours géré par cette application (deuxième liste de liens "Jupyter nbgrader")

A noter qu'un enseignant ne verra dans ces listes que les cours dans lesquels il est déclaré, même si l'application de gestion gère d'autres cours.

#### Ecran de Gestion d'un Cours
Dans l'application de gestion de cours, voici l'écran de gestion d'un cours en particulier:

 ![EcranGestionUnCours](./images/EcranGestionUnCours.png)

Dans cet écran, la partie *"Configuration (auto) des étudiants du cours"* permet de gérer quels étudiants auront accès aux TP de ce cours, voir section *"Définir la Liste des Etudiants d'un Cours"* pour son utilisation.

La partie *"sélection du TP"* permet d'accéder à l'écran d'évaluation des TPs du cours.

#### Ecran d'Evaluation des TPs d'un Cours

![EvaluationTP](./images/EvaluationTP.png)

Cet écran permet de récupérer les TPs soumis par les étudiants. Une fois les TPs des étudiants collectés, le bouton *"Bilan TP"* permet d'avoir une vue de toutes les soumissions. Voir section *"Cycle de Vie d'un TP"*.

## Guides Pratiques
### Ajouter un Enseignant à un Cours

1. **Modifier hub_config.py**
   Dans l'espace jupyter du cours, modifier le fichier **hub_config.py** pour ajouter le login ucbl de l'enseignant à la liste (faire attention à ce que ce soit bien son login, notamment pour les personnes avec des noms exotiques). 

   ![hubconfig](./images/hubconfig.png)

2. **Attendre une journée** pour que les changements prennent effet, généralement vers 3/4 heures du matin.

### Définir la Liste des Etudiants d'un Cours
Chaque cours peut être soit ouvert à tous les étudiants, soit réservé à un ensemble particulier d'étudiant. Ajouter un étudiant à un cours ne veut pas dire qu'il aura accès à l'espace jupyter du cours. Cela veut dire qu'il recevra les TPs émis par le cours, et pourra les rendre après avoir travaillé dessus.

1. Accéder à l'écran de gestion du cours

![ListeEtudiant](./images/ListeEtudiant.png)

2. Cliquer sur le bouton **"etudiants.txt"**

![FichierListeEtudiant](./images/FichierListeEtudiant.png)

3. Editer le fichier texte pour y ajouter les identifiants des étudiants (par exemple en copiant-collant depuis Tomuss)
4. Sauvegarder avec ctrl-S
5. Dans l'écran de gestion du cours, cliquer sur le bouton **"config. auto"** et attendre que le script s'exécute
6. Une page va s'afficher avec le résultat du script. Si *"bash script execution OK"* est indiqué en haut, les changements se sont bien passés. Il est possible de revenir à l'écran précédent grâce au bouton retour du navigateur web
7. Le troisième bouton **"reset acl"** permet de rendre le cours accessible à tout les étudiants

### Cycle de Vie d'un TP
#### Création
Dans l'espace Jupyter du cours, entrez dans le dossier *"source"* et créez un dossier pour contenir le TP (exemple: *07_TP_Oscillations*). En général, évitez les espaces dans les noms de dossier/fichier.

Dans ce dossier, créez le notebook du TP (exemple: *TP_Oscillations.ipynb*), ainsi que les fichiers annexes (images, jeux de données...). Tout le contenu de ce dossier sera distribué aux étudiants.
#### Déclaration
Pour que le TP soit reconnu par la plateforme, il faut le déclarer en créant un fichier de configuration dans le dsossier source (ou se trouve les TP) de l'espace jupyter du cours. Dans notre exemple, il faut créer un fichier *07_TP_Oscillations.cfg* qui ressemble à cela:

```python
[IPYNB]
nomipynb = TP_Oscillations.ipynb
cellids = cell_all
[PDF]
nompdf = 
[TEX]
nomtex =
[BIB]
nombib = 
exos = 
direxos = 
[PGE]
nompge =
[MALUS]
similaire = 0
taux = 0.8
[NOTE]
base = 0.0
bib  = 0.0
ipynb= 0.7
```
La ligne importante est la première, qui indique le nom du notebook du TP.

#### Distribution aux Etudiants
Dans l'espace Jupyter du cours, cliquez sur le bouton *"Formgrader"* pour accéder à l'écran de gestion des TPs de ce cours.

![GestionTP](./images/GestionTP.png)

Dans cet écran :

1. Cliquez sur le bouton *"Generate"* de la ligne du TP à distribuer. Cela va notamment créer une version étudiante du notebook de TP en ajoutant le contenu du notebook *"header.ipynb"* (situé à la racine de l'espace Jupyter du cours) en haut du notebook du TP.
2. Une fois les fichiers à distribuer générés, cliquez sur le bouton *"Release"* pour en ouvrir l'accès aux étudiants.

#### Le Point de Vue d'un Etudiant
L'étudiant se connecte à la plateforme Jupyter de la même façon qu'un enseignant (login/mot de passe ucbl). Il accède ainsi à son espace personnel Jupyter.

![EspaceJupyterEtudiant](./images/EspaceJupyterEtudiant.png)

##### Récupération
L'étudiant clique sur le bouton *"Assignments"* de son espace Jupyter pour accéder à l'écran de gestion de ses TPs.

![AssignmentEtudiant](./images/AssignmentEtudiant.png)

Après avoir sélectionné le cours correspondant dans le menu déroulant (ici, *PHY2020*), l'étudiant peut récupérer les TPs de son choix en cliquant sur le bouton *"fetch"*. A noter que cela copie chez l'étudiant l'intégralité du dossier du TP et non pas seulement le notebook.
Après avoir récupéré un TP, celui ci apparait dans la liste *"Downloaded assignments"*.

![AssignmentEtudiant2](./images/AssignmentEtudiant2.png)

##### Modification
Le TP est maintenant présent dans l'espace Jupyter de l'étudiant. Celui-ci peut travailler dessus et le modifier à loisir.
##### Soumission
Une fois le TP finit, l'étudiant retourne dans le menu *"Assignments"* et clique le bouton *"Submit"* pour rendre son TP. Cela rend l'intégralité du dossier du TP et non pas seulement le notebook.
L'étudiant peut rendre le même TP plusieurs fois (par exemple après avoir fait une modification après l'avoir rendu une première fois). L'interface montre tout les rendus, ainsi que les moments auxquels ils ont eus lieux dans la partie *"Submitted Assignments"*.

![AssignmentEtudiant](./images/AssignmentEtudiant3.png)

#### Récupération des Soumissions des Etudiants
Après que les étudiants aient rendus leurs TPs, l'enseignant peut les récupérer dans l'écran dévaluation du TP. Pour s'y rendre à partir de l'espace Jupyter du cours: 

1. Bouton Control Panel
2. Bouton Services
3. Application de Gestion de Cours 
4. Cliquer sur le cours contenant le TP
5. Cliquer sur le TP dans la partie "sélection du TP"

![EvaluationTP](./images/EvaluationTP.png)

L'enseignant doit ensuite:

1. Récupérer les TPs des étudiants en cliquant sur le bouton *"Validation auto"* (et attendre la fin de l'exécution du script correspondant)
2. Une page s'affiche avec les informations de debug du script, vérifier la mention "bash script execution OK" en haut, puis utiliser le bouton retour du navigateur pour revenir à la page précédente.
3. Cliquer sur le bouton *"Bilan TP"*. La page suivante s'affiche:

![BilanSoumissionTP](./images/BilanSoumissionTP.png)

Cette page montre d'abords la liste des étudiants ayant rendu un TP puis, pour chaque étudiant:

- La date de rendu du TP
- Un lien vers le dossier contenant ce qu'a rendu l'étudiant (c'est une copie de ce qui se trouvait dans le dossier de TP de l'étudiant quand il l'a rendu)
- Un lien pour voir le notebook du TP rendu par l'étudiant au format HTML
- Un indice entre 0 (complètement différent) et 1 (identique) illustrant la similarité textuelle de ce qu'a rendu l'étudiant avec ce qu'ont rendu les autres étudiant. L'indice qui est présenté est l'indice de similarité avec l'autre rendu étudiant le plus similaire. Sur la même ligne, il y a également un lien vers l'étudiant dont le rendu est le plus similaire. Un indice en dessous de 0.8 est normal, un indice au dessus de 0.8 est suspicieux.

