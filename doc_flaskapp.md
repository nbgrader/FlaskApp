---
title: Documentation sur les outils de validation
---
# FlaksApp: gestion de cours et validation

## Développement

Cette application a été développée sous <a href="https://linux.org/">Linux <i class="fab fa-linux"></i></a> avec <a href="https://python.org">Python <i class="fab fa-python" aria_hidden="true"></i></a> et 
<a href="https://flask.palletsprojects.com/">Flask <i class="fas fa-flask"></i></a>  par <a href="https://perso.univ-lyon1.fr/marc.buffat/">Marc BUFFAT (département mécanique, UCB Lyon 1)</a> au départ pour des besoins pédagogiques personnels. 
Ardent partisan du logiciel libre et de Linux, il a souhaité la partager pour promouvoir l'utilisation des notebooks Jupyter dans l'enseignement supérieur.

Cette application utilise le principe de base d'Unix KISS: **Keep It Simple, Stupid!**

Elle consiste en un ensemble de scripts shell ou de programmes Python, utilisés en ligne de commande pour analyser, évaluer et noter les TP des étudiants. Pour rendre plus accessible ces outils, une interface Web a été développée avec le framework Flask en python, avec des liens avec Jupyterhub et nbgrader. La conception reste modulaire avec la possibilité d'ajouter des fonctionnalités.


## Principe

Les cours, dont le nom correspond au code APOGEE (noté MGCxxx pour les cours en mécanique) sont regroupés dans des meta cours (notés: cours, cours1 ,...).
La gestion se fait au niveau de ces meta cours, ce qui permet de regrouper tous les cours d'une année de formation, d'une équipe pédagogique. 
Un cours peut correspondre à l'ensemble d'un cours, ou découpé par groupe de TD ou de TP. Dans ce cas on peut partager les ressources, la différence étant la liste des étudiants de chaque cours. On note dans ce cas les cours MGCxxxG1, MGCxxxG2 .. .

Un exemple de structure est donnée sur le schéma suivant:

- structure des cours 

<p><span style="display:block;background:lightyellow;">
        <b>cours/</b><br>
	├── IntroPython<br>
	├── MGC1061M<br>
	├── MGC2005L<br>
	├── MGC2014L<br>
	├── MGC2367MG1<br>
	├── MGC2367MG2<br>
	├── MGC3001L<br>
	├── MGC3062L<br>
	├── MGCtest<br>
	├── PL9014ME<br>
	├── PythonL1<br>
	└── PythonScientifique<br>
        <b>cours1/</b><br>
	├── DEMO<br>
	└── FlaskApp<br>
	<br>
</p>

Pour chaque cours, on a une équipe pédagogique formée d'un ou plusieurs enseignants: le responsable du cours, le responsable du TP et les autres enseignants (intervenants dans les autres groupes de TP). Les membres de l'équipe pédagogique peuvent générer toutes les évaluation du cours.

Chaque cours est constitué de plusieurs TP, qui sont des documents (notebook, fichiers, data,..) regroupés dans un même dossier et que l'on échange avec les étudiants.
 
On peut limiter l'accès à un cours en indiquant dans le fichier **validation/etudiants.txt**  la liste des numéros d'étudiants 
autorisés à suivre le cours. On peut l'obtenir par exemple par copier coller à partir des tables  [TOMUSS](https://perso.univ-lyon1.fr/thierry.excoffier/TOMUSS/home.html).
 
On peut aussi laisser le cours accessible à tous, et générer la liste des étudiants à partir des devoirs rendus.

La validation opère sur chaque TP indépendamment avec les documents fournit par les étudiants. Le travail demandé étant personnel, on a un système 
de détection de plagiat entre étudiants. Le principe de la validation dépend de ce que l'on demande aux étudiants et des documents qui sont fournit par les étudiants. Le principe de base est de ne pas transférer les fichiers, et de faire la partie d'évaluation en ligne avec un simple navigateur et l'accès à une interface web. Le résultat peut ensuite être éventuellement récupéré, soit sous la forme d'un fichier standard (pdf), soit comme un fichier de notes au format standard  csv (compatible [**tomuss**](https://tomuss.univ-lyon1.fr/), soit copier directement sous **tomuss** avec la fonction *importer des valeurs* avec un copier-coller.

On génère un fichier de bilan par TP qui permet à l'enseignant d'accéder simplement à tous les résultats de l'étudiant sous forme standard html sans avoir à convertir les fichiers, ni exécuter les notebooks. En utilisant en partie l'évaluation automatique avec nbgrader, on peut compléter cette évaluation par l'analyse des commentaires des étudiants dans les notebooks ou des fichiers de comptes rendus au format pdf.

Pour tester le plagiat, les documents (notebook, fichier python, fichier tex pour les comptes rendus) sont tout d'abord convertit en fichier texte en ne conservant que les réponses des étudiants puis comparer entre eux à l'aide d'une métrique de Levenshtein avec ou sans filtrage de mots clés. Le résultat est une table de similitude avec des valeurs de 0 a 1 dont on extrait la plus grande valeur sur chaque ligne. Si cette valeur est plus grande qu'une valeur type (de l'ordre de 0.8), alors on considère qu'un potentiel plagiat a pu avoir lieu entre les 2 étudiants, et l'enseignant peut alors comparer les 2 copies. Pour les notebooks, on extrait uniquement les réponses des étudiants dans certaines cellules.

### Notebook

L'étudiant doit compléter un notebook à trou fournit par l'enseignant. C'est l'utilisation la plus simple: l'enseignant crée son notebook avec des cellules vides et demande aux étudiants de répondre. A la fin l'enseignant aura alors la charge de l'évaluation complète du travail, comme un compte rendu de TP classique.

En utilisant le système **nbgrader** ( voir la documentation [**nbgrader**](https://nbgrader.readthedocs.io/en/stable/) ), l'enseignant peut modifier les attributs de chaque cellule du notebook en sélectionnant  le menu: 

- Affichage -> Barre d'outil de cellule -> Create Assignement 

Une barre d'outil apparaît alors en haut de chaque cellule avec un numéro de cellule ID (générer automatiquement mais modifiable) et un type choisit à partit d'un menu:

**\-**
 : type par défaut

**Manually graded answer**
 : permet de déclarer la cellule comme contenant une réponse de l'étudiant devant être évaluer manuellement. Dans ce cas peut lui attribuer des points

**Manually graded task**
 : identique mais pour une tâche (voir la documentation)

**Read-only**
 : la cellule n'est pas modifiable par l'étudiant. Typiquement elle contient les instructions ou du code fournit par l'enseignant.

Si de plus la cellule est une cellule de code, alors on a en plus les 2 choix suivants:

**Autograded answer**
: l'étudiant doit entrer du code python dans la cellule qui sera ensuite tester automatiquement dans la cellule suivante. 
L'enseignant peut entrer une solution entre les 2 balises **### BEGIN SOLUTION**  et **### END SOLUTION**, qui ne sera pas vu par l'étudiant mais qui permet à l'enseignant de tester le notebook en particulier les tests des cellules **autograder test**

**Autograder test**
: dans cette cellule l'enseignant test le résultat de l'étudiant en utilisant la fonction python **assert()** . Si l'étudiant passe les tests avec succès il a le nombre de points choisit dans la barre de menu.


## Configuration


**convention**
: on utilise les conventions Unix pour les noms et les répertoires (dossiers), sans utilisation d'espace ou d'accents dans le nom (on peut avantageusement remplacer l'espace par l'underscore '_').

### définition

**cours**
: Un cours est associé à un nom (par convention son code APOGEE) p.e. **MGCtest**

**TP**
: chaque cours est constitué de plusieurs TP, qui sont des documents (notebook, fichiers, data,..) que l'on échange avec les étudiants avec la commande fetch et la commande submit. Dans le menu Assignments  la première ligne indique le nom du cours sélectionné et en dessous la liste des TP publiés (released). Un TP est associé à un nom de TP (avec la convention Unix) qui correspond au nom du répertoire contenant tous les documents. 

**fetch**
: la commande fetch de l'interface jupyter nbgrader Assignemnts->Fetch permet aux étudiants de récupérer les documents d'un TP.

**submit**
: la commande submit de l'interface jupyter nbgrader Assignemnts->Submit permet aux étudiants de soumettre le résultat de leur travail sur un  TP.

**etudiant**
: les étudiants sont identifiés par leur identifiant de connexion, i.e. leur numéro d'étudiant avec le premier chiffre remplacé par un p, p.e. **p1801574** pour un numéro d'étudiant *11801574*


**enseignant**
: un enseignant a un statut d'étudiant par défaut, ce qui lui permet de tester l'interface étudiant

**administrateur**
: pour pouvoir administrer/modifier/gérer un cours, il faut avoir un statut administrateur, qui permet d'avoir accès au service du cours avec l'interface jupyter nbgrader: Control Panel -> Services.  
Plusieurs enseignants (équipe enseignante) peuvent accéder à l'administration d'un même cours.
C'est à l'équipe pédagogique de gérer le partage des taches entre les enseignants. 

La suite de la documentation s'adresse aux enseignants ayant se rôle administrateur de cours.

## Structure d'un cours

Un cours sous Jupyter nbgrader est associé à un répertoire sur le serveur. Le nom du cours est par convention son code APOGEE. Par exemple le répertoire du cours **MGCtest** a la structure suivante

- répertoire du cours **MGCtest**

<p><span style="display:block;background:lightyellow;">
	<b>MGCtest/</b><br>
	├── <i>autograded</i><br>
	│   ├── p1801574<br>
	│   │   └── TP4_Meule<br>
	│   ├── p1811042<br>
	│   │   └── TP4_Meule<br>
	│   └── p2006068<br>
	│       &nbsp;&nbsp;&nbsp; └── TP4_Meule<br>
	├── <i>release</i><br>
	│   ├── TP4_Meule<br>
	│   └── TP5_Bille<br>
	├── <b>source</b><br>
	│   ├── TP4_Meule<br>
	│   └── TP5_Bille<br>
	├── <i>submitted</i><br>
	│   ├── p1801574<br>
	│   │   └── TP4_Meule<br>
	│   ├── p1811042<br>
	│   │   ├── TP3_Toupie3D<br>
	│   │   ├── TP4_Meule<br>
	│   │   └── TP5_Bille<br>
	│   └── p2006068<br>
	│       &nbsp;&nbsp;&nbsp; ├── TP4_Meule<br>
	│       &nbsp;&nbsp;&nbsp; └── TP5_Bille<br>
	└── <b>validation</b><br>
</span></p>

Les répertoires en italique sont crées automatiquement par **nbgrader**

*release*
: contient les TP mis en ligne par l'enseignant responsable du cours ( menu Formgrader de l'interface jupyter nbgrader)

*submitted*
: contient les TP qui ont été soumis par les étudiants. Pour chaque étudiant on trouve les TP soumis par cet étudiant

*nbgrader*
: contient le résultat de l'évaluation des TP par le système nbgrader, avec la même structure par étudiant

Les répertoires en gras sont générés par l'administrateur

- **source** contient les TP que l'enseignant fournit aux étudiants (dans l'exemple TP4_Meule et TP5_Bille) (ainsi que les fichier de configuration **TPxx.cfg**)
- **validation** contient les résultats  de l'analyse avec les outils de validation pour chacun des TP
- **bib** répertoire optionnel pour les bibliothèques partagées avec les étudiants
- **cours** répertoire  (optionnel) utilisé par l'enseignant en cours 
- **cours_html** répertoire (optionnel) contenant une documentation (en html) à destination des étudiants
- **SONDAGE** répertoire (optionnel) pour l'utilisation de l'application de sondage basée sur le module activity des notebooks jupyter.
- 
## fichiers de configuration


<p><span style="display:block;background:lightyellow;">
	<b>MCCtest/</b><br>
	├── autograded<br>
	├── gradebook.db<br>
	├── <b>header.ipynb</b><br>
	├── <i>hub_config.py</i><br>
	├── <i>nbgrader_config.py</i><br>
	├── README.notes<br>
	├── release<br>
	├── source<br>
	├── submitted<br>
	├── <b>source/TP4_Meule.cfg</b><br>
	├── <b>source/TP5_Bille.cfg</b><br>
	└── validation<br>
</span></p>

Le répertoire du cours contient des fichiers de configuration système en italique, que l'on ne modifie en générale pas

*nbgrader_config.py*:
: définit le nom du cours et l'entête des notebooks (par défaut *header.ipynb*)

*hub_config.py*
: définit le service du cours, avec la liste des enseignants administrateur du cours (on peut rajouter des noms)

Les fichiers de configuration en gras sont définit par l'enseignant

**header.ipynb**
: notebook ipython d'entête pour tous les notebooks du cours

**source/TPxx.cfg**
:  fichier de configuration pour chacun des TP du cours que l'on veut soumettre au système de validation (TPxx vaut dans l'exemple TP4_Meule.cfg et TP5_Bille.cfg). Ce fichier n'est obligatoire que si l'on veut utiliser le système de validation. Pour une utilisation de nbgrader de base, ce fichier n'est pas nécessaire. Il est maintenant dans le répertoire source avec le répertoire du TP.


## fichier TPxx.cfg

La structure d'un fichier de configuration pour le système de validation suit la convention standard de la bibliothèque Python ConfigParser. ConfigParser est une classe Python qui implémente un langage de configuration de base pour les programmes Python. Il fournit une structure similaire aux fichiers INI de Microsoft Windows. Pour les TP que l'on veut distribuer aux étudiants sans évaluation du travail, ce fichier n'est pas obligatoire.

Par exemple le contenu du fichier **TP4_Meule.cfg** est le suivant:

~~~
[IPYNB]
nomipynb = TP_Meule.ipynb
cellids = cell_all
argpdf = --rm 3
[PDF]
nompdf = 
[TEX]
nomtex =
[BIB]
nombib = 
exos = 
direxos = 
[PGE]
nompge =
[MALUS]
similaire = 7
taux = 0.85
[NOTE]
rendu = 0.0
bib  = 0.0
ipynb= 1.0
auto = 1.0
manu = 0.0
~~~

Les différentes sections entre [] sont obligatoires, ainsi que tous les mots clés de configuration, mais leurs valeurs peuvent être vide.

Par contre si on crée un fichier de configuration vide, le système en crée automatiquement un avec des valeurs par défaut lorsque l'on sélectionne le TP pour la première fois.

**section [IPYNB]**
: définit le nom du notebook ou des notebooks du TP (séparés par une virgule "," sans espace) dans le mot clé **nomipynb**. 

Le mot clé **cellids** permet de définir les cellules du notebook qui seront soumis au système de plagiat.
Les valeurs possibles sont:

  ** ** aucun test de plagiat
- **cells** (toutes les cellules du notebook). On génère une version markdown du notebook, qui sert au plagiat
- **cell_all** pour toutes les cellules solutions, 
- **cell_code** pour uniquement les cellules de code, 
- **cell_markdown** pour uniquement les cellules de texte, 
- une liste de numéro ID de cellules.

Dans le cas **cells**, une version html et markdown du notebook est générée. On peut donc indiquer ces fichiers dans le fichier de configuration.

Le mot clé **argpdf** permet de spécifier des paramètres pour la génération du pdf des notebooks:

- **\-\-no-input**       enlève le code des cellules
- **\-\-no-output**      enlève la sortie des cellules
- **\-\-pandoc**         utilisation de pandoc
- **\-\-cellid CELLID**  id des cellules à afficher (=cells/cell_all/cell_code/cell_markdown)
- **\-\-rm RM\_CELLS**    nbre de cellules a supprimer au debut du notebook


**section [TEX]**
: définit le nom optionnel du fichier source latex ou markdown pour le compte rendu dans **nomtex**. L'enseignant fournir le canevas LaTex ou markdown de base. Ce fichier sera soumis au test de similitude s'il existe.

**section  [PDF]**
: définit le nom optionnel du fichier pdf ou html dans **nompdf**, correspondant à la version pdf ou html du rapport obtenu avec pdflatex ou pandoc pour lecture par l'enseignant.

**section [BIB]**
: définit le nom optionnel dans **nombib**  du fichier de bibliothèque python que l'étudiant doit écrire. Ce fichier sera soumis au test de similitude s'il existe. Dans **exos** on peut spécifier un dictionnaire de la forme **val1:prog1,val2:prog2,..**, ou prog1 est le nom d'un programme que l'étudiant doit écrire dans sa bibliothèque et val1 est le nom du programme de validation associé définit dans la bibliothèque validation. Dans le cas où on donne des exercices à faire tirés aléatoirement dans une banque d'exercices, on spécifie alors le nom du répertoire de configuration des exercices dans **direxos**. 

**section [PGE]**
: définit le nom optionnel dans **nompge**  du ou des fichiers programmes python que l'étudiant doit rendre.  Ce fichier sera soumis au test de similitude s'il existe.

**section [MALUS]**
: définit la pénalité de malus sur la note dans **similaire** en cas de similitude trop importante. Le taux de similitude limite (de l'ordre de 0.8) est donné dans **taux**.

Si le taux de similarité **simil** est supérieur à la valeur de **taux** alors la note est remplacée par le calcul suivant fonction du taux de similitude (simil):
 
        note * (1  - taux * (simil - similaire)/(1 - similaire))

de même la note de rendu du devoir (égale à 1 par défaut) est amendée ans la même proportion

**section [NOTE]**
: permet de définir le calcul de la notation automatique avec la formule

**note = rendu x nbre de fichier rendu + bib x note_autograde_bibilotheque + ipynb x note_autograde_notebook - malus**

si l'étudiant n'a pas rendu le TP alors sa notes est **ABINJ**

Si on souhaites prendre en compte une notation manuelle, les 2 derniers mots clés **auto** et **manu** définissent le poids de la note automatique (calculé précédemment: **note**) et la note manuelle dans la note finale: 

**note_finale = auto*note + manu*note_manu**


## Interface Web

A partir du serveur jupyter-nbgrader du cours, on se connecte à l'interface web du système de validation, pour avoir accès aux différents outils de validation et aux résultats. En haut de la page on a accès à un menu permettant de choisir les différentes étapes. Cette interface est par défaut en mode **simplifiée**. Pour passer en mode **détaillée**, 
il suffit de cliquer sur le bouton **interface détaillée** sur la page de gestion des cours (et inversement pour revenir mode simplifiée)

Cette interface a été développée en python avec le framework Flask et exécute les différents scripts bash ou python pour effectuer ces différentes étapes.

![Interface Flask](./images/validationUI.png)

### <Jupyter

Retour sur le service jupyter nbgrader du cours

### Gestion 

Cette page contient la liste des cours dans lesquels on fait partie de l'équipe pédagogique. 

Dans la première liste, on a l'accès au cours avec **nbgrader**, pour définir son contenu en cliquant simplement sur le nom du cours.

Dans la seconde liste, on va gérer le cours, en choisissant tout d'abord le cours parmi cette liste de cours que l'on administre sur le serveur.

### Cours

Pour le cours sélectionner, on va gérer le cours ainsi que les TP qui ont été configuré dans ce cours. On peut visualiser et modifier les fichiers de configuration TPxxx.cfg et on a un accès directe au dossier de ces TP (sous jupyterhub), que l'on peut donc modifier aussi.

#### configuration automatique
On définit juste la liste des étudiants inscrits au cours avec leurs numéros étudiants dans le fichier **etudiants.txt** (fichier vide si cours accessible à tout le monde)

On clique ensuite sur le bouton **config auto** pour ajouter automatiquement les étudiants dans la bd nbgrader et autoriser cette liste d'étudiants d'accéder aux TP.

La gestion des étudiants dans un cours limite l'accès au cours aux seuls étudiants inscrits. On utilise pour cela les ACL (access control list) sur les répertoires d'échange utilisées par jupyter nbgrader. Ces droits sont gérés au niveau des groupes pour éviter la limitation des ACL.
 On crée donc un groupe Unix ayant le même nom que le cours. L'autorisation se fait ensuite par groupe en ajoutant les étudiants et l'équipe enseignante du cours à ce groupe.
Il n'y a alors plus de limitation de taille. Les commandes Unix utilisées dans le script de configuration automatique sont les suivantes:

          groupadd MGC2028L
          usermod -a -G MGC2028L pxxxxx 

Ces commandes nécessitent des droits administrateurs et il faut donner les droits au compte du meta-cours pour les exécuter (dans /etc/sudoers.d/jupyter).

#### configuration manuelle

On peut une liste des étudiants avec uniquement les étudiants ayant soumis au moins un TP dans le cours, ou se trouvant déjà dans la base de données du cours.

1. sélection du TP
2. édite/modifie les fichiers de configuration des TP
3. accès aux dossiers de chaque TP
4. création de la liste des comptes étudiants pour les TP

   - a partir de la liste des étudiants inscrits 
   - création / importation de la liste des numéros étudiants inscrits (à partir de tomuss) dans **etudiants.txt**
   - affiche la liste des étudiants ayant soumis au moins un notebook
   - affiche la liste des étudiants dans la bd du cours

5. gestion des droits des étudiants

   - `config. auto`: on n'autorise que les étudiants inscrits (dans le fichier **etudiants.txt**)
   - `config. reset`: on autorise tous les étudiants 

6. archivage de la validation

**attention** on ne conserve qu'une seule archive pour des questions de place disque ! 
Pensez à les télécharger pour les conserver si vous le souhaitez !

   - archivage de la validation dans un fichier archive **validation_mois_annee.tar.gz**
   - purge d'un cours en supprimant tous les travaux rendus, la base de données du cours (à faire quand on commence une nouvelle session de cours)

7. équipe enseignante

   - liste des enseignants pouvant gérer un cours et ayant accès au cours. 

### TP

Cette page permet de visualiser/configurer/évaluer un TP du cours.

#### Collecte des TP étudiants

Pour évaluer le travail des étudiants, on récupère les fichiers
des étudiants à partir du répertoire d'échange **nbgrader** pour les copier dans le répertoire **submitted** du cours. Pour cela il suffit de cliquez sur le bouton **Collecte TP** (à faire en générale une seul fois après la date limite de soumission du TP)

#### Validation automatique

On clique sur le bouton **Validation auto** pour effectuer la validation autograde du TP pour tous les étudiants inscrits (attention peut être long!!)

On a ensuite accès au bilan au format html, et à la note de chaque étudiant exportable sous tomuss


**Remarque** par défaut on ne génère pas le rapport en pdf de chaque étudiant (uniquement en html)

#### configuration de l'évaluation

  - accès en édition au fichier de configuration du TP pour paramétrer les notations.

#### génération (manuelle) de l'évaluation 

1. Configuration

  - accès en édition à la liste des étudiants qui seront évalués. Attention cette liste contient uniquement le nom de connexion de l'étudiant
  
2. Génération de l'évaluation
  - récupération du travail des étudiants avec nbgrader 

       - sans autograde
       - avec autograde
       - avec autograde et génération du rapport pdf de chaque étudiant

  - création/mise à jour de la liste des étudiants à partir des étudiants inscrits ou ayant soumis leur travail
  - génération du bilan d'évaluation pour tous les étudiants. Attention cette partie peut être longue suivant le nombre d'étudiants (de quelques minutes à plusieurs dizaines de minutes). Il faut attendre le résultat sous peine d'interrompre l'analyse.

#### bilan de l'évaluation

L'évaluation génère plusieurs fichiers, dont un fichier de bilan au format html (ou pdf), un fichier de notes au format csv et un fichier de similitude au format csv.

0. Liste des étudiants évalués
  - accès à la liste des étudiants qui ont été évalués.
 
1. Bilan de l'évaluation
  - accès au bilan d'évaluation du TP pour tous les étudiants au format html ou pdf avec des liens vers le travail individuel de chaque étudiant

2. Accès aux notes automatiques par étudiants (fichier csv)
  - accès au bilan des notes des étudiants, avec visualisation du fichier de notes, exportation vers tomuss ou édition du fichier brute csv

3. Notation manuelle des étudiants (avec des commentaires éventuels)
  - création/mise à jour du fichier de notes manuels (une note par étudiant) et calcul de la note finale
  - accès au fichier csv et exportation sous tomuss des notes et/ou des commentaires (copier coller dans les tables tomuss)

4. Accès au fichier csv de similitude par étudiant
  - accès au fichier de similitude avec pour chaque étudiant et chaque document (notebook, rapport, librairie python, programme) un taux maximum de similitude avec le numéro de l'étudiant correspondant

6. Accès aux dossiers des TP des étudiants
  - accès au répertoire du TP
  - accès au répertoire de validation

#### TP virtuel

La gestion de TP virtuels utilisant la bibliothèque Python **[streamlit](https://streamlit.io/)** est maintenant possible sous l'interface de gestion de cours.

##### Principe

L'enseignant écrit un programme Python pour traiter et analyser de données. A partir de ce programme et en utilisant la bibliothèque Python open-source **[streamlit](https://streamlit.io/)**, il peut rapidement créer et partager une application web personnalisée pour l'apprentissage automatique et la science des données. Une fois validé, il peut partager cette application avec ses étudiants sous la forme d'un *TP virtuel* sur le serveur de cours JupyterHub.

##### Mise en œuvre

Le fichier source du TP virtuel, p.e. `TPvirtuel.py` est déposé sur le serveur du cours, p.e. `MGCtest`, dans le sous-répertoire `cours` (i.e. `MGCtest/cours/TPvirtuel.py`).

Dans le répertoire  `cours`, on définit la configuration par défaut du serveur streamlit dans le fichier ` .streamlit/config.toml `.

~~~
[browser]
gatherUsageStats = false
[server]
~~~

Dans le répertoire `source`, on crée un fichier  `TPvirtuel.toml`, qui peut être vide, pour indiquer au système de gestion de cours que l'on souhaite gérer le TP virtuel `TPvirtuel.py`.

Dans l'interface de gestion de cours, ce TP virtuel apparaît alors dans la section TPvirtuel, en en cliquant dessus, l'enseignant peut le démarrer, l'arrêter ou obtenir son statut.

L'enseignant crée ensuite un notebook Jupyter avec les explications pour les étudiants. Il peut générer
automatiquement un lien d'accès au TP en utilisant la fonction python `link_tpvirt`de la bibliothèque de validation:

         link_tpvirt()

On peut éventuellement spécifier des paramètres supplémentaires (avec 
l'argument nommé `params`) qui seront passés au programme de TP virtuel.

**Remarque**  le compte de l'étudiant est passé dans l'URL et peut donc être récupéré par le programme en utilisant la fonction streamlit

         st.experimental_get_query_params() 

**Attention**: chaque fois que le serveur apache redémarre, cela arrête tous les TP virtuels en cours, mais
ceux ci sont normalement redémarrer lors du rédémarrage du serveur apache.

##### Mise en place d'un proxy sous le serveur apache

Par défaut, l'application streamlit utilise un port (en général 8501) pour son serveur web. Les règles de sécurité peuvent empêcher l'accès à ce port en dehors de l'intranet de l'établissement.

On peut cependant configurer le serveur apache, pour que ce port soit rediriger vers une url accessible, 
par exemple  http://nom_machine/tp/nom_cours

La configuration apache suivante permet de gérer 2 ports avec 2 applications streamlit: 

- un pour des sondages (accessible avec `http://nom_machine/sondage/nom_cours`) 
- un pour des TP (`http://nom_machine/tp/nom_cours`)

**attention** accès en http (et pas https)

~~~
<VirtualHost *:80>
	ServerName mbuffat-nbgrader.univ-lyon1.fr
	ServerAlias mbuffat-nbgrader.univ-lyon1.fr

	ServerAdmin marc.buffat@univ-lyon1.fr
	DocumentRoot /var/www/html/
	# streamlit
	ProxyPreserveHost On
	ProxyRequests Off
	<Proxy *>
   		Order deny,allow
   		Allow from all
	</Proxy>
   	<Location "/sondage/">
   		ProxyPass  http://localhost:8500/
   		ProxyPassReverse  http://localhost:8500/
   	</Location>
   	<Location "/sondage/stream">
      		ProxyPass ws://localhost:8500/stream
      		ProxyPassReverse ws://localhost:8500/stream
   	</Location>
   	<Location "/tp/">
   		ProxyPass  http://localhost:8501/
   		ProxyPassReverse  http://localhost:8501/
   	</Location>
   	<Location "/tp/stream">
      		ProxyPass ws://localhost:8501/stream
      		ProxyPassReverse ws://localhost:8501/stream
   	</Location>
	# fin steamlit
	
</VirtualHost>
~~~

##### Gestion (alternative) avec systemctl

Cette solution de gestion des TP virtuels est indépendante du serveur apache, mais un peu plus complexe.

Cette solution nécessite une configuration pour lancer le TP virtuel sous systemd. Il faut tout d'abord 
autoriser le méta cours à utiliser la gestion des services utilisateurs `systemctl --user` avec la commande 
root

         sudo loginctl enable-linger cours


Il faut ensuite créer le service dans le répertoire `~/.config/systemd/user`,
par exemple TPDataStreamlit.service:

~~~
[Unit]
Description=TP virtuel TPDataStreamlit

[Service]
ExecStart=/home/cours/FlaskApp/bin/tpvirt_service.sh TPDataStreamlit
WorkingDirectory=/home/cours/MGC3062L/cours
~~~

On peut ensuite gérer le TP virtuel avec les commandes suivantes dans un shell bash

~~~
# force utilisation de bash
bash
# status du service
XDG_RUNTIME_DIR=/run/user/$UID systemctl --user status TP6DataStreamlit.service
# demarrage du service
XDG_RUNTIME_DIR=/run/user/$UID systemctl --user start TP6DataStreamlit.service
# arret du service
XDG_RUNTIME_DIR=/run/user/$UID systemctl --user stop TP6DataStreamlit.service
~~~


### Docs
Regroupe la documentation avec 

#### Guide enseignant

Guide d'utilisation et documentation sur cette interface de **jupyter / nbgrader**

#### Documentation

Lien vers cette documentation créer avec **pandoc** en **markdown**

#### FAQ

Lien vers une série de questions courantes avec les réponses

#### About

Information sur la configuration du serveur avec en particulier les ressources matérielles (nombre de coeurs, mémoire) et logicielles (liste des bibliothèques python installées)

### Admin

outils d'administration avec

- l'accès aux services jupyter actifs sur le serveur, avec possibilité d'accès aux notebooks et d'arrêt du service (réservé aux administrateurs)
- l'état du (ou des ) serveurs jupyterhub avec la charge et le nombre d'utilisateurs actifs (réservé aux administrateurs)
- l'accès aux notebooks (et dossiers) des étudiants incrits à un cours (réservé aux enseignants du cours) 
- l'accès aux outils de monitoring [Prometheus / Grafana](https://prometheus.io/docs/visualization/grafana/)
 
## Outils en ligne de commande

### bilanTP

Ce programme python permet une analyse et une notation automatique des TP d'un cours. Il utilise le fichier de configuration du TP (fichier .cfg) et s'exécute à la racine du cours. C'est ce programme qui est utilisé dans l'interface web, mais il peut être utilisé en ligne de commande en ouvrant un terminal dans le cours. 

Par exemple pour évaluer le TP: TP1 du cours MGC2367M:

`cours@m2-nbgrader:~/MGC2367M$  bilanTP -b validation -l validation/etudiants.txt TP1`

La syntaxe d'utilisation est la suivante


<pre>
bilanTP -h

usage: bilanTP [-h] [-d] [-a] [--bilan] [--pdf] [--pandoc]
               [--simil [LISTE_COURS [LISTE_COURS ...]]] [-l LISTE_ETU]
               [-b BASE_DIR]
               nom_TP

analyse et validation de TP notebooks de cours

positional arguments:
  nom_TP                nom du TP

optional arguments:
  -h, --help            show this help message and exit
  -d, --debug           mode debug
  -a, --autograde       autograde des notebooks
  --bilan               creation fichier de bilan des notebooks
  --pdf                 sortie pdf des notebooks
  --pandoc              sortie pdf des notebooks avec pandoc
  --simil [LISTE_COURS [LISTE_COURS ...]]
                        liste des cours pour test de similitude uniquement
  -l LISTE_ETU          fichier contenant les comptes étudiants à évaluer
  -b BASE_DIR           repertoire de base pour creer les fichiers
</pre> 

### validation d'exercices python

#### valide_exo
validation automatique d'une série d'exercices python tirés aléatoirement dans une bibliothèque. Le système utilise un répertoire commun `/usr/local/commun/.validation/` dans lequel on configure des exercices pour un TP donné dans un fichier validation.conf:

<pre>
buffat@p2chpd-visu2:/usr/local/commun/.validation$ ls MGC1061M_TP1
buffat.cfg  buffat.py  p1518926.cfg  p1518926.py  validation.conf
</pre>

L'étudiant peut ensuite utiliser cette commande pour valider son exercice.

La syntaxe de la commande est la suivante:

<pre>
valide_exo -h
syntaxe pour valide_exo :
     valide_exo [-n nomTP] [-d] [-t] [-f] init monfichier.py
               initialise les exercices a valider dans un fichier python monfichier.py (-t test uniqt, -f efface et recree)
     valide_exo [-n nomTP] [-d] info
               affiche la configuration
     valide_exo [-n nomTP] [-s] list
               liste les exercices a valider (-s short list)
     valide_exo [-n nomTP] [-d] [-t] exo mafonction [option]
               valide la fonction mafonction pour l'exercice exo (-t test uniqt sans mise a jour resultat)
     valide_exo help
               affiche les consignes
     valide_exo plagiat 
               affiche les rêgles sur le plagiat

il vaut mieux spécifier explicitement le nom du TP nomTP, ce qui évite de le spécifier dans le fichier de configuration dans .validation
</pre>

Le fichier de configuration `validation.cfg` permet de spécifier la date, la durée et la liste des exercices à valider par les étudiants. Dans la liste des exercices, on peut spécifier un exercice, ou une série d'exercices dans laquelle sera tirée au hasard l'exercice à réaliser par l'étudiant.

<pre>
[CONFIG]
date = 2019/03/07 20:50
enseignant = cours
nomtp = MGC1061M_TP1
duree = 10080

[EXERCISE]
dir = /usr/local/commun/.validation/MGC1061M_TP1
section = exo18,S50,S50,S60,S60,S70,S80,S80,S80,S90,S90
rmexos = 
noms = 
</pre>
#### test_exo

permet de tester individuellement un exercice python (dans une bibliothéque)

La syntaxe est la suivante:
<pre>
test_exo -h
syntaxe : test_exo [-d/-l sect/-s ] fichier nom_exo fonction [arg]
     -d      : mode DEBUG (option)
     -s      : liste des sections
     -l [sec]: liste des exos de la section sec (option)
     -e [exo]: affiche l'exo
     fichier : nom du fichier python (avec extension .py) 
     nom_exo : nom de l'exo pour la validation (ex exo11)
     fonction: nom de la fonction à valider
     arg_exo : argument exercise (option)
</pre>

#### bilan_exo
génération d'un bilan de validation des exercices de valide_exo pour tous les étudiants

la syntaxe est la suivante:

<pre>
syntaxe: bilan_exo [-d] [-t/--tomuss] [-l/--levenshtein] [-r/--rep directory] NOMTP [fichier]

 -d              : debug
 -t/--tomuss     : exportation pour tomuss (uniqt etudiant et remplace p par 1)
 -l/--levenshtein: indice de levenshtein au lieu de jaccard (par defaut)
 -i/--indice levelmin:  niveau minimum de similitude prise en compte (def. 80)
 -r/--rep        : directory remplace le repertoire par default 
 -h/--help       : aide

 NOMTP 	     : nom du TP (repertoire) 
 fichier.csv : fichier resultat (et similitude) au format csv (sans extension)
 genere aussi un fichier tar avec tous les fichiers etudiants

</pre>

## Contact

Si vous constatez des erreurs, ou des informations manquantes, n'hésitez pas à contacter l'auteur par mél:
 <a href="https://perso.univ-lyon1.fr/marc.buffat/">Marc BUFFAT (département mécanique, UCB Lyon 1)</a> 

## Licence

Projet développé sous Licence Libre.
Les logiciels sont libres d'utilisation, mais sans garantie!

\(C\) Marc BUFFAT, Dpt Mécanique, Université Lyon 1


