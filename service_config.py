import os
import sys

c = get_config()

name2 = 'Validation_API'
c.JupyterHub.services.append(
    {
        'name': name2,
        'api_token': 'X8m5h64J8q',
        'admin': True,
        'display': False,
    })

name3 = 'whoami'
c.JupyterHub.services.append(
    {
        'name': name3,
        'url': 'http://127.0.0.1:10103',
        'display': False,
    	'oauth_no_confirm': True,
    	'user': 'cours',
   	    'cwd': "/home/cours/FlaskApp/",
        'command': ['flask', 'run', '--port=10103'],
        'environment': {
            'FLASK_APP': 'whoami_flask.py',
        }
    })
