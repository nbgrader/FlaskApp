# Système de gestion de cours avec "FlaskApp"

FlaskApp est un  ensemble d'outils de gestion de cours utilisant Jupyter nbgrader avec une interface WEB développée sous Flask et 
implémentée comme service sous Jupyterhub.

Ces applications ont été développées sous Linux  avec Python  et Flask  au départ pour des besoins pédagogiques personnels.

## Principe

Cette application utilise le principe de base d'Unix KISS: Keep It Simple, Stupid!

Elle consiste en un ensemble de scripts shell ou de programmes Python, utilisés en ligne de commande pour analyser, évaluer et noter les TP des étudiants. Pour rendre plus accessible ces outils, une interface Web a été développée avec le framework Flask en python, avec des liens avec Jupyterhub et nbgrader. La conception reste modulaire avec la possibilité d'ajouter des fonctionnalités.

L'application est installée comme un service avec authentification sous jupyterhub, permettant aux équipes pédagogiques d'accéder
à l'application  à travers le web.

## Dépendance

Les packages suivants sont nécessaire:

  - jupyter, jupyterlab, jupyterhub, nbgrader, nbconvert
  - pandoc, latex
  - Flask 

L'application flask utilise le système de log sous systemd, avec la bibliothèque  suivante

      pip install systemd-logging

## Configuration

Cloner le dépôt git avec

       git clone  https://forge.univ-lyon1.fr/nbgrader/FlaskApp.git

Il faut ensuite créer 2 fichiers de configuration **config.py** , **jupyterhub_config.py** à partir des modèles config-default.py et jupyterhub_config.py.org
en modifiant le nom du gestionnaire de cours et les cours gérés. Concernant config.py, l'application lira config-default.py puis config.py, remplaçant/ajoutant les valeurs trouvées.

 - config.py : fichier de configuration de l'application flask  (void documentation [https://flask.palletsprojects.com/en/2.1.x/config/](https://flask.palletsprojects.com/en/2.1.x/config/)
 
ce fichier contiens aussi des parametres de configuration en particulier pour les cours gérés:

-  `COURS` : liste des cours (potentiels) à gérer
- `BASEDIR`: répertoire de base des cours
- `SECRET_KEY`: clé d'accès au service  jupyterhub
- `HOSTS`: liste des serveurs jupyterhub à monitorer (en plus du serveur local)
 
 Dans le répertoire `instance` se trouve aussi un lien symbolique vers ce fichier `config.py`.

 Il faut ensuite configurer le service jupyterhub permettant l'accès à l'application à partir du serveur jupyterhub avec le nom du compte et le port à utiliser. Il faut ensuite créer un lien symbolique dans le répertoire de configuration de jupyterhub (`$VIRTUAL_ENV/etc/jupyter/conf.d`) 

Dans le répertoire `static`, il faut aussi créer des liens symboliques vers les cours (répertoires) gérés par l'application

Cette application est exécutée par un serveur apache, que l'on doit configurer avec :

 - l'installation d'un mode `wsgi` pour lancer les applications flasks (dans mod-enabled)
 - la configuration de chaque application de gestion de cours dans `conf-available` (on doit spécifier le lien vers le fichier flaskapp.wsgi qui 
exécute ensuite le programme flaskapp.py, et le compte qui exécute l'application = gestionnaire de cours). Un exemple est disponible dans le dossier exemples : flask_cours-l2physique.conf
 - la création d'un lien symbolique dans le dossier `conf-enabled`, pointant vers le fichier de configuration écrit à l'étape précédente en utilisant la commande `a2enconf`

## Documentation

- [documentation des outils de gestion/validation](documentation.md)
- [documentation nbgrader](doc_nbgrader.md)
- [FAQ](faq.md)

## Authors and acknowledgment

Marc BUFFAT, dpt mécanique, Université Lyon 1

## Licence

Projet sous licence libre, mais sans garantie

## Project status

Projet en cours de développement

